using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class UIWiki : MonoBehaviour
{
    [Header("Wiki Button")]
    [SerializeField] private Image btnTunicaFibrosa;
    [SerializeField] private Image btnTunicaVascular;
    [SerializeField] private Image btnRetina;
    [SerializeField] private Image btnMediosTransparentes;
    [Header("Sprite Buttons")]
    [SerializeField] private Sprite btnEnableTunicaFibrosa;
    [SerializeField] private Sprite btnDisableTunicaFiborsa;
    [SerializeField] private Sprite btnEnableTunicaVascular;
    [SerializeField] private Sprite btnDisableTunicaVascular;
    [SerializeField] private Sprite btnEnableRetina;
    [SerializeField] private Sprite btnDisableRetina;
    [SerializeField] private Sprite btnEnableMediosTransparentes;
    [SerializeField] private Sprite btnDisableMediosTransparentes;
    [Header("Scrolls")]
    [SerializeField] private GameObject svTunicaFibrosa;
    [SerializeField] private GameObject svTunicaVascular;
    [SerializeField] private GameObject svRetina;
    [SerializeField] private GameObject svMediosTransparentes;
    [Header("Toggles Tunica Fibrosa")]
    [SerializeField] private Toggle tgTunicaFibrosa;
    [SerializeField] private Toggle tgCornea;
    [SerializeField] private Toggle tgEsclera;
    [SerializeField] private Toggle tgMusculosExtraoculares;
    [Header("Toggle Tunica Vascular")]
    [SerializeField] private Toggle  tgTunicaVascular;
    [Header("Toggle Retina")]
    [SerializeField] private Toggle tgRetina;
    [SerializeField] private Toggle tgZonasRetina;
    [Header("Toggles Medios Transparentes")]
    [SerializeField] private Toggle tgSegmentoAnterior;
    [SerializeField] private Toggle tgCristalino;
    [SerializeField] private Toggle tgHumorVitreo;
    [Header("Componentes Tunica Fibrosa")]
    [SerializeField] private GameObject cmpTunicaFibrosa;
    [SerializeField] private GameObject cmpCornea;
    [SerializeField] private GameObject cmpEsclera;
    [SerializeField] private GameObject cmpMusculosExtraoculares;
    [Header("Componentes Tunica Vascular")]
    [SerializeField] private GameObject cmpTunicaVascular;
    [Header("Componentes Retina")]
    [SerializeField] private GameObject cmpRetina;
    [SerializeField] private GameObject cmpZonasRetina;
    [Header("Componentes Medios Transparentes")]
    [SerializeField] private GameObject cmpSegmentoAnterios;
    [SerializeField] private GameObject cmpCristalino;
    [SerializeField] private GameObject cmpHumorVitreo;

    public void SwitchSprite(string value){
        switch(value){
            case "TunicaFibrosa":
            btnTunicaFibrosa.sprite = btnEnableTunicaFibrosa;
            btnTunicaVascular.sprite = btnDisableTunicaVascular;
            btnRetina.sprite = btnDisableRetina;
            btnMediosTransparentes.sprite = btnDisableMediosTransparentes;

            svTunicaFibrosa.SetActive(true);
            svTunicaVascular.SetActive(false);
            svRetina.SetActive(false);
            svMediosTransparentes.SetActive(false);

            ResetToggles(false);
            ResetComponentes(false);

            break;
            case "TunicaVascular":
            btnTunicaFibrosa.sprite = btnDisableTunicaFiborsa;
            btnTunicaVascular.sprite = btnEnableTunicaVascular;
            btnRetina.sprite = btnDisableRetina;
            btnMediosTransparentes.sprite = btnDisableMediosTransparentes;

            svTunicaFibrosa.SetActive(false);
            svTunicaVascular.SetActive(true);
            svRetina.SetActive(false);
            svMediosTransparentes.SetActive(false);

            ResetToggles(false);
            ResetComponentes(false);

            break;
            case "Retina":
            btnTunicaFibrosa.sprite = btnDisableTunicaFiborsa;
            btnTunicaVascular.sprite = btnDisableTunicaVascular;
            btnRetina.sprite = btnEnableRetina;
            btnMediosTransparentes.sprite = btnDisableMediosTransparentes;

            svTunicaFibrosa.SetActive(false);
            svTunicaVascular.SetActive(false);
            svRetina.SetActive(true);
            svMediosTransparentes.SetActive(false);

            ResetToggles(false);
            ResetComponentes(false);

            break;
            case "MediosTransparentes":
            btnTunicaFibrosa.sprite = btnDisableTunicaFiborsa;
            btnTunicaVascular.sprite = btnDisableTunicaVascular;
            btnRetina.sprite = btnDisableRetina;
            btnMediosTransparentes.sprite = btnEnableMediosTransparentes;

            svTunicaFibrosa.SetActive(false);
            svTunicaVascular.SetActive(false);
            svRetina.SetActive(false);
            svMediosTransparentes.SetActive(true);

            ResetToggles(false);
            ResetComponentes(false);

            break;
        }
            
    }

    public void ResetToggles(bool value){
        tgTunicaFibrosa.isOn = value;
        tgCornea.isOn = value;
        tgEsclera.isOn = value;
        tgMusculosExtraoculares.isOn = value;
        tgTunicaVascular.isOn = value;
        tgRetina.isOn = value;
        tgZonasRetina.isOn = value;
        tgSegmentoAnterior.isOn = value;
        tgCristalino.isOn = value;
        tgHumorVitreo.isOn = value;
    }
    public void ResetComponentes(bool value){
        cmpTunicaFibrosa.SetActive(value);
        cmpCornea.SetActive(value);
        cmpEsclera.SetActive(value);
        cmpMusculosExtraoculares.SetActive(value);
        cmpTunicaVascular.SetActive(value);
        cmpRetina.SetActive(value);
        cmpZonasRetina.SetActive(value);
        cmpSegmentoAnterios.SetActive(value);
        cmpCristalino.SetActive(value);
        cmpHumorVitreo.SetActive(value);
    }
    // Start is called before the first frame update
    void Start()
    {
        SwitchSprite("TunicaFibrosa");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
