using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UI;

public class CameraHandler : MonoBehaviour
{
    [SerializeField] private GameObject camera3D;
    [SerializeField] private GameObject cameraAR;
    [SerializeField] private GameObject cameraARFijo;

    [Header("Image Button Cameta")]
    [SerializeField] private Image btn3D;
    [SerializeField] private Image btnAR;
    [SerializeField] private Image btnARFijo;
    [Header("Camera Offset")]
    [SerializeField] private Transform offset3D;
    [SerializeField] private Transform offsetAR;
    [SerializeField] private Transform offsetARFijo;
    [Header("Modelos 3D")]
    [SerializeField] private GameObject globoOcular;
    [SerializeField] private GameObject globoSagital;

    [Header("Scripts")]
    [SerializeField] private UIHeader uIHeader;

    private bool isARFijo =true;
    public bool isAR = false;

    // Start is called before the first frame update
    void Start()
    {
        CameraMode("ARFijo");
        CentrarModelos();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CameraMode(string mode)
    {
        switch (mode)
        {
            case "3D":
                isARFijo = false;
                isAR = false;
                camera3D.SetActive(true);
                cameraAR.SetActive(false);
                btn3D.enabled = true;
                btnAR.enabled = false;
                btnARFijo.enabled = false;
                globoOcular.transform.SetParent(offset3D);
                globoSagital.transform.SetParent(offset3D); 

                CentrarModelos();
                break;
            case "AR":
                isARFijo = false;
                isAR = true;
                camera3D.SetActive(false);
                cameraAR.SetActive(true);
                btn3D.enabled = false;
                btnAR.enabled = true;
                btnARFijo.enabled = false;
                globoOcular.transform.SetParent(offsetAR);
                globoSagital.transform.SetParent(offsetAR);

                CentrarModelos();
                break;
            case "ARFijo":
                isARFijo = true;
                isAR = false;
                camera3D.SetActive(false);
                cameraAR.SetActive(true);
                btn3D.enabled = false;
                btnAR.enabled = false;
                btnARFijo.enabled = true;
                globoOcular.transform.SetParent(offsetARFijo);
                globoSagital.transform.SetParent(offsetARFijo);

                CentrarModelos();
                break;
        }
    }

    public void CentrarModelos()
    {
        if(isARFijo == true)
        {
            globoOcular.transform.localPosition = Vector3.zero;
            globoSagital.transform.localPosition = Vector3.zero;
            globoOcular.transform.rotation = Quaternion.identity;
            globoSagital.transform.rotation = Quaternion.identity;
            globoOcular.transform.localScale = Vector3.one;
            globoSagital.transform.localScale = Vector3.one;
            


        }
        else
        {
            globoOcular.transform.localPosition = Vector3.zero;
            globoSagital.transform.localPosition = Vector3.zero;
            globoOcular.transform.rotation = Quaternion.identity;
            globoSagital.transform.rotation = Quaternion.identity;
            globoOcular.transform.localScale = Vector3.one;
            globoSagital.transform.localScale = Vector3.one;


        }
    }
    public void RetinaModelPositions(string vista)
    {
        switch (vista)
        {
            case "OraSerrata":
                globoOcular.transform.localPosition = new Vector3(0f, 0.004706576f, 0f);
                globoSagital.transform.localPosition = new Vector3(0f, 0.0044f, 0f);
                globoOcular.transform.rotation =  Quaternion.Euler(0, 90, 0);
                globoSagital.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
                globoOcular.transform.localScale = new Vector3(1.087805f, 1.087805f, 1.087805f);
                globoSagital.transform.localScale = new Vector3(1.113376f, 1.113376f, 1.113376f);
                break;
            case "Macula":
                globoOcular.transform.localPosition = new Vector3(0f, 0f, 0f);
                globoSagital.transform.localPosition = new Vector3(-0.22f, 0f, 0f);
                globoOcular.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                globoSagital.transform.rotation = Quaternion.Euler(0f, 35f, 0f);
                globoOcular.transform.localScale = new Vector3(5f, 5f, 5f);
                globoSagital.transform.localScale = new Vector3(5f, 5f, 5f);
                break;
        }
    }
    public void MoveRightOffSet(bool state){
        if (state)
        {
            offsetARFijo.localPosition = new Vector3(0.1f, -0.02f, 0.65f);
            offset3D.localPosition = new Vector3(0.1f, -0.02f, 0.65f);
            offsetARFijo.rotation = Quaternion.Euler(0f, 6.402f, 0f);
            offset3D.rotation = Quaternion.Euler(0f, 6.402f, 0f);
        }
        else
        {
            offsetARFijo.localPosition = new Vector3(0.0001f, -0.02f, 0.65f);
            offset3D.localPosition = new Vector3(0.0001f, -0.02f, 0.65f);
            offsetARFijo.rotation = Quaternion.Euler(0f, 0f, 0f);
            offset3D.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }
    public void MoveLeftOffSet(bool state){
        if (state)
        {
            offsetARFijo.localPosition = new Vector3(-0.1f, -0.02f, 0.65f);
            offset3D.localPosition = new Vector3(-0.1f, -0.02f, 0.65f);
            offsetAR.rotation = Quaternion.Euler(0f, 6.402f, 0f);
            offset3D.rotation = Quaternion.Euler(0f, 6.402f, 0f);
        }
        else
        {
            offsetARFijo.localPosition = new Vector3(0.0001f, -0.02f, 0.65f);
            offset3D.localPosition = new Vector3(0.0001f, -0.02f, 0.65f);
            offsetARFijo.rotation = Quaternion.Euler(0f, 0f, 0f);
            offset3D.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }


}
