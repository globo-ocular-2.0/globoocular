using UnityEngine;

public class JoystickIntraOcularesAnimationController : MonoBehaviour
{
    public UIVistasMovimientosOculares uIVistasMovimientosOculares;
    public Animator animatorGlobo; // Referencia al Animator
    public Animator animatorSagital;
    public string upAnimationName = "UpAnim"; // Nombre de la animación para arriba
    public string downAnimationName = "DownAnim"; // Nombre de la animación para abajo
    public float verticalLimit = 100f; // Límite vertical para el joystick (arriba/abajo)

    private string currentAnimationName = ""; // Animación actual
    private float currentNormalizedTime = 0f; // Tiempo normalizado actual

    /// <summary>
    /// Actualiza el progreso de la animación basado en la posición del joystick en el eje Y.
    /// </summary>
    public void UpdateAnimationProgress(float joystickY)
    {
        // Normalizar el valor del joystick para el eje Y
        float normalizedY = Mathf.InverseLerp(0, verticalLimit, joystickY); // De 0 a arriba
        float normalizedY2 = Mathf.InverseLerp(0, -verticalLimit, joystickY); // De 0 a abajo

        if (joystickY > 0)
        {
            PlayAnimation(upAnimationName, normalizedY); // Animación de subir
            uIVistasMovimientosOculares.AnimLabelMovimientos("pupilaDilatada");
        }
        else if (joystickY < 0)
        {
            PlayAnimation(downAnimationName, normalizedY2); // Animación de bajar
            uIVistasMovimientosOculares.AnimLabelMovimientos("pupilaContraida");
        }
        else
        {
            PlayAnimation("", 0f); // No hay movimiento (centro)
        }
    }

    /// <summary>
    /// Reproduce una animación específica en un tiempo normalizado.
    /// </summary>
    private void PlayAnimation(string animationName, float normalizedTime)
    {
        if (currentAnimationName != animationName || Mathf.Abs(currentNormalizedTime - normalizedTime) > 0.01f)
        {
            currentAnimationName = animationName;
            currentNormalizedTime = normalizedTime;

            if (!string.IsNullOrEmpty(animationName))
            {
                // Establecer la animación al tiempo normalizado sin reiniciarla
                animatorGlobo.Play(animationName, 0, normalizedTime);
                animatorSagital.Play(animationName, 0, normalizedTime);
                animatorGlobo.speed = 0f; // Detener el avance automático de la animación
                animatorSagital.speed = 0f;
            }
        }
    }

    /// <summary>
    /// Reinicia las animaciones al estado neutro.
    /// </summary>
    public void ResetAnimation()
    {
        uIVistasMovimientosOculares.SetActiveLabelMovimientosExtraOculares(false);
        currentAnimationName = "";
        currentNormalizedTime = 0f;
        animatorGlobo.speed = 1f; // Volver a la velocidad normal
        animatorSagital.speed = 1f;
    }
}
