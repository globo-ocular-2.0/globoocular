using UnityEngine;
using UnityEngine.UI;

public class JoystickAnimationController : MonoBehaviour
{
    public UIVistasMovimientosOculares uIVistasMovimientosOculares;
    public Animator animator; // Referencia al Animator
    public Toggle toggle;
    public string leftAnimationName = "LeftAnim"; // Nombre de la animación izquierda
    public string rightAnimationName = "RightAnim"; // Nombre de la animación derecha
    public string upAnimationName = "UpAnim";
    public string downAnimationName = "DownAnim";
    public string leftUpAnimationName = "LeftUpAnim"; // Nombre de la animación izquierda + arriba
    public string leftDownAnimationName = "LeftDownAnim"; // Nombre de la animación izquierda + abajo
    public string rightUpAnimationName = "RightUpAnim"; // Nombre de la animación derecha + arriba
    public string rightDownAnimationName = "RightDownAnim"; // Nombre de la animación derecha + abajo
    public float joystickMinX = -200f; // Límite mínimo del joystick (izquierda)
    public float joystickMaxX = 200f; // Límite máximo del joystick (derecha)
    public float verticalLimit = 100f; // Límite vertical para el joystick (arriba/abajo)

    private string currentAnimationName = ""; // Animación actual
    private float currentNormalizedTime = 0f; // Tiempo normalizado actual

    public void UpdateAnimationProgress(float joystickX, float joystickY)
    {
        // Normalizar el valor del joystick al rango de 0 a 1 para el eje X
        float normalizedX = Mathf.InverseLerp(joystickMinX, 0, joystickX);
        float normalizedX2 = Mathf.InverseLerp(0, joystickMaxX, joystickX);

        // Normalizar el valor del joystick para el eje Y
        float normalizedY = Mathf.InverseLerp(verticalLimit, 0, joystickY);
        float normalizedY2 = Mathf.InverseLerp(0, -verticalLimit, joystickY);

        // Si el joystick está en el rango de movimiento en X
        if (joystickX < joystickMinX || joystickX > joystickMaxX)
        {
            // Cuando el joystick está al extremo (máximo o mínimo en el eje X)
            if (joystickY > 0)
            {
                PlayAnimation(joystickX < 0 ? leftUpAnimationName : rightUpAnimationName, normalizedY); // Arriba
            }
            else if (joystickY < 0)
            {
                PlayAnimation(joystickX < 0 ? leftDownAnimationName : rightDownAnimationName, normalizedY2); // Abajo
            }

            else
                PlayAnimation(joystickX < 0 ? leftAnimationName : rightAnimationName, normalizedY); // Solo X, cuando Y es 0
        }
        else
        {
            // Movimiento en el eje X: realizar animación dinámica con respecto al joystick en X
            if (joystickX < 0)
            {
                // Movimiento a la izquierda
                if (joystickY > 0)
                {
                    PlayAnimation(leftUpAnimationName, normalizedY); // Izquierda + Arriba
                    uIVistasMovimientosOculares.AnimLabelMovimientos("dextroSupraversion");
                }   
                else if (joystickY < 0)
                {
                    PlayAnimation(leftDownAnimationName, normalizedY2); // Izquierda + Abajo
                    uIVistasMovimientosOculares.AnimLabelMovimientos("dextroInfraversion");
                }
                else
                {
                    PlayAnimation(leftAnimationName, normalizedX); // Solo Izquierda
                    if(toggle.isOn == false)
                    uIVistasMovimientosOculares.AnimLabelMovimientos("abduccion");
                }
                    
            }
            else if (joystickX > 0)
            {
                // Movimiento a la derecha
                if (joystickY > 0)
                {
                    PlayAnimation(rightUpAnimationName, normalizedY); // Derecha + Arriba
                    uIVistasMovimientosOculares.AnimLabelMovimientos("levosupraversion");
                }
                else if (joystickY < 0)
                {
                    PlayAnimation(rightDownAnimationName, normalizedY2); // Derecha + Abajo
                    uIVistasMovimientosOculares.AnimLabelMovimientos("levoinfraversion");
                }
                else
                {
                    PlayAnimation(rightAnimationName, normalizedX2); // Solo Derecha
                    if(toggle.isOn == false)
                    uIVistasMovimientosOculares.AnimLabelMovimientos("aduccion");
                }
            }
            else
            {
                if (joystickY > 0)
                {
                    PlayAnimation(upAnimationName, normalizedY); // Arriba
                    uIVistasMovimientosOculares.AnimLabelMovimientos("supraversion");
                }
                else if (joystickY < 0)
                {
                    PlayAnimation(downAnimationName, normalizedY2); // Abajo
                    uIVistasMovimientosOculares.AnimLabelMovimientos("infraversion");
                }
                else
                {
                    PlayAnimation("", 0f); // No hay movimiento (centro)
                }
            }
            
            
        }
    }


    private void PlayAnimation(string animationName, float normalizedTime)
    {
        // Si la animación o el progreso cambian, actualizamos
        if (currentAnimationName != animationName || Mathf.Abs(currentNormalizedTime - normalizedTime) > 0.01f)
        {
            currentAnimationName = animationName;
            currentNormalizedTime = normalizedTime;

            // Establecer la animación al tiempo normalizado sin reiniciarla
            animator.Play(animationName, 0, normalizedTime);
            animator.speed = 0f; // Detener el avance automático de la animación
        }
    }

    public void ResetAnimation()
    {
        uIVistasMovimientosOculares.SetActiveLabelMovimientosExtraOculares(false);
        // Resetear al centro/neutro cuando se suelta el joystick
        currentAnimationName = "";
        currentNormalizedTime = 0f;
        animator.speed = 1f; // Volver a la velocidad normal (opcional)
    }
}
