using UnityEngine;
using UnityEngine.EventSystems;

public class VerticalJoystick : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public RectTransform background; // Fondo del joystick
    public RectTransform handle;     // Joystick (manejador)

    [Header("Movimiento")]
    public float handleRange = 100f;  // Radio de movimiento del joystick
    public float joystickMinY = -100f; // Rango mínimo en Y (abajo)
    public float joystickMaxY = 100f;  // Rango máximo en Y (arriba)

    private Vector2 inputVector; // Vector de entrada

    public JoystickIntraOcularesAnimationController animationController; // Referencia al controlador de animaciones

    public void OnDrag(PointerEventData eventData)
    {
        // Convertir la posición del toque a coordenadas locales
        Vector2 position = RectTransformUtility.WorldToScreenPoint(null, background.position);
        Vector2 direction = eventData.position - position;

        // Normalizar entrada
        inputVector = direction / handleRange;
        inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

        // Movimiento vertical restringido
        float newY = Mathf.Clamp(inputVector.y * handleRange, joystickMinY, joystickMaxY);

        // Actualizar posición del joystick (X fijo en 0, solo se mueve en Y)
        handle.anchoredPosition = new Vector2(0, newY);

        // Actualizar animaciones
        if (animationController != null)
        {
            animationController.UpdateAnimationProgress(newY);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        // Resetear joystick al soltarlo
        inputVector = Vector2.zero;
        handle.anchoredPosition = Vector2.zero;

        // Reiniciar animaciones
        if (animationController != null)
        {
            animationController.ResetAnimation();
        }
    }
}
