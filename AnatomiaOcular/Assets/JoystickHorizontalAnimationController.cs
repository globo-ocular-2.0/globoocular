using UnityEngine;

public class JoystickHorizontalAnimationController : MonoBehaviour
{
    public UIVistasMovimientosOculares uIVistasMovimientosOculares;
    public Animator animatorGlobo; // Referencia al Animator
    public Animator animatorSagital;
    public string rightAnimationName = "CristalinoExpandir"; // Nombre de la animación para la derecha
    public string leftAnimationName = "CristalinoContraer";   // Nombre de la animación para la izquierda
    public float horizontalLimit = 100f; // Límite horizontal para el joystick (derecha/izquierda)

    private string currentAnimationName = ""; // Animación actual
    private float currentNormalizedTime = 0f; // Tiempo normalizado actual

    /// <summary>
    /// Actualiza el progreso de la animación basado en la posición del joystick en el eje X.
    /// </summary>
    public void UpdateAnimationProgress(float joystickX)
    {
        // Normalizar el valor del joystick para el eje X
        float normalizedX = Mathf.InverseLerp(0, horizontalLimit, joystickX);  // De 0 a derecha
        float normalizedX2 = Mathf.InverseLerp(0, -horizontalLimit, joystickX); // De 0 a izquierda

        if (joystickX > 0)
        {
            PlayAnimation(rightAnimationName, normalizedX); // Animación hacia la derecha
            uIVistasMovimientosOculares.AnimLabelMovimientos("abombado");
        }
        else if (joystickX < 0)
        {
            PlayAnimation(leftAnimationName, normalizedX2); // Animación hacia la izquierda
            uIVistasMovimientosOculares.AnimLabelMovimientos("aplanar");
        }
        else
        {
            PlayAnimation("", 0f); // No hay movimiento (centro)
        }
    }

    /// <summary>
    /// Reproduce una animación específica en un tiempo normalizado.
    /// </summary>
    private void PlayAnimation(string animationName, float normalizedTime)
    {
        if (currentAnimationName != animationName || Mathf.Abs(currentNormalizedTime - normalizedTime) > 0.01f)
        {
            currentAnimationName = animationName;
            currentNormalizedTime = normalizedTime;

            if (!string.IsNullOrEmpty(animationName))
            {
                // Establecer la animación al tiempo normalizado sin reiniciarla
                animatorGlobo.Play(animationName, 0, normalizedTime);
                animatorSagital.Play(animationName, 0, normalizedTime);
                animatorGlobo.speed = 0f; // Detener el avance automático de la animación
                animatorSagital.speed = 0f;
            }
        }
    }

    /// <summary>
    /// Reinicia las animaciones al estado neutro.
    /// </summary>
    public void ResetAnimation()
    {
        uIVistasMovimientosOculares.SetActiveLabelMovimientosExtraOculares(false);
        currentAnimationName = "";
        currentNormalizedTime = 0f;
        animatorGlobo.speed = 1f; // Volver a la velocidad normal
        animatorSagital.speed = 1f;
    }
}
