using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ModelViewManager : MonoBehaviour
{
    [SerializeField] private GameObject[] tunicaFibrosaModels;
    [SerializeField] private GameObject[] tunicaVascularModels;
    [SerializeField] private GameObject[] retinaModels;
    [SerializeField] private GameObject[] mediosTransparentesModels;
    

    // Arreglo de arreglos para manejar todos los modelos
    private GameObject[][] allModelArrays;

    private void Start()
    {
        // Inicializa el arreglo de arreglos
        allModelArrays = new GameObject[][]
        {
            tunicaFibrosaModels,
            tunicaVascularModels,
            retinaModels,
            mediosTransparentesModels
        };
    }

    public void ActiveAllModels(bool state)
    {
        for (int i = 0; i < allModelArrays.Length; i++)
        {
            for (int j = 0; j < allModelArrays[i].Length; j++)
            {
                allModelArrays[i][j].SetActive(state);
            }
        }
    }

    // Función para desactivar todos los objetos y activar uno específico
    public void ActivateModelSet(int selectedIndex)
    {
        // Verifica que el índice esté dentro del rango
        if (selectedIndex < 0 || selectedIndex >= allModelArrays.Length)
        {
            Debug.LogError("Índice fuera de rango");
            return;
        }

        // Desactivar todos los objetos en todos los arreglos
        ActiveAllModels(false);

        // Activar solo los objetos en el arreglo seleccionado
        foreach (var model in allModelArrays[selectedIndex])
        {
            model.SetActive(true);
        }
    }

    // Función para activar o desactivar un componente específico por índices
    public void SetComponentState(int arrayIndex, int componentIndex, bool state)
    {
        // Verifica que el índice del array esté dentro del rango
        if (arrayIndex < 0 || arrayIndex >= allModelArrays.Length)
        {
            Debug.LogError("Índice del arreglo fuera de rango");
            return;
        }

        // Verifica que el índice del componente esté dentro del rango
        if (componentIndex < 0 || componentIndex >= allModelArrays[arrayIndex].Length)
        {
            Debug.LogError("Índice del componente fuera de rango");
            return;
        }

        // Activa o desactiva el componente específico
        allModelArrays[arrayIndex][componentIndex].SetActive(state);
    }
    public void SetTunicaFibrosaCornea(bool value) => SetComponentState(0, 0, value);
    public void SetTunicaFibrosaEsclera(bool value) => SetComponentState(0, 1, value);
    public void SetTunicaFibrosaMusculo1(bool value) => SetComponentState(0, 2, value);
    public void SetTunicaFibrosaMusculo2(bool value) => SetComponentState(0, 3, value);
    public void SetTunicaFibrosaMusculo3(bool value) => SetComponentState(0, 4, value); 
    public void SetTunicaFibrosaMusculo4(bool value) => SetComponentState(0, 5, value);
    public void SetTunicaFibrosaMusculo5(bool value) => SetComponentState(0, 6, value);
    public void SetTunicaFibrosaMusculo6(bool value) => SetComponentState(0, 7, value);

    public void SetTunicaVascularIris(bool value) => SetComponentState(1, 0, value);
    public void SetTunicaVascularProcesosCiliares(bool value) => SetComponentState(1, 1, value);
    public void SetTunicaVascularCoroides(bool value) => SetComponentState(1, 2, value);

    public void SetRetinaRetina(bool value) => SetComponentState(2, 0, value);
    public void SetRetinaProcesosCiliares(bool value) => SetComponentState(2, 1, value);
    public void SetRetinaAteriaCentral(bool value) => SetComponentState(2, 2, value);
    public void SetRetinaVenaCentral(bool value) => SetComponentState(2, 3, value);

    public void SetMediosTransparentesCornea(bool value) => SetComponentState(3, 0, value);
    public void SetMediosTransparentesCristalino(bool value) => SetComponentState(3, 1, value);
    public void SetMediosTransparentesHumorAcuoso(bool value) => SetComponentState(3, 2, value);
    public void SetMediosTransparentesHumorVítreo(bool value) => SetComponentState(3, 3, value);
}
