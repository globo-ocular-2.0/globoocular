using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleColor : MonoBehaviour
{
    [SerializeField] private Toggle toggle;
    [SerializeField] private Sprite eyeClose;
    [SerializeField] private Sprite eyeOpen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }



    public void ToggleColorChange()
    {
        if (toggle.isOn)
        {
            GetComponent<Image>().color = Color.green;
            transform.GetChild(0).GetComponent<Image>().sprite = eyeOpen;
            
            print("Toggle is on");
        }
        else
        {
            GetComponent<Image>().color = Color.white;
            transform.GetChild(0).GetComponent<Image>().sprite = eyeClose;
            print("Toggle is off");
        }
    }
}
