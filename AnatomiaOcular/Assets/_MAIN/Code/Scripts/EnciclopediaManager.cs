using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EnciclopediaManager : MonoBehaviour
{   [Header("Enciclopedia Manager")]

    [SerializeField] private TMP_Text textHeaderEnciclopedia;

    [Header("PreSet Toggle Groups")]
    [SerializeField] private Toggle[] tunicaFibrosaToggles;
    [SerializeField] private Toggle[] tunicaVascularToggles;
    [SerializeField] private Toggle[] retinaToggles;
    [SerializeField] private Toggle[] estructurasTransparentesToggles;

    [Header("PreSet Model Groups")]
    [SerializeField] private GameObject[] tunicaFibrosaModels;
    [SerializeField] private GameObject[] tunicaVascularModels;
    [SerializeField] private GameObject[] retinaModels;
    [SerializeField] private GameObject[] estructurasTransparentesModels;


    private Dictionary<string, Toggle[]> toggleGroups;
    private Dictionary<string, (int start, int end)> toggleRanges;
    private Dictionary<string, GameObject[]> modelGroups;
    private Dictionary<string, (int start, int end)> modelRanges;

    private void Awake()
    {
        InitializeToggleGroups();
        InitializeToggleRanges();
        InitializeModelGroups();
        InitializeModelRanges();
    }

    private void InitializeToggleGroups()
    {
        toggleGroups = new Dictionary<string, Toggle[]>
        {
            { "TunicaFibrosa", tunicaFibrosaToggles },
            { "TunicaVascular", tunicaVascularToggles },
            { "Retina", retinaToggles },
            { "EstructurasTransparentes", estructurasTransparentesToggles }
        };
    }

    private void InitializeToggleRanges()
    {
        toggleRanges = new Dictionary<string, (int start, int end)>
        {
            { "TunicaFibrosaAll", (1, tunicaFibrosaToggles.Length) },
            { "MusculosOculares", (4, tunicaFibrosaToggles.Length) },
            { "TunicaVascularAll", (1, tunicaVascularToggles.Length) },
            { "CuerpoCiliar", (3, 5) },
            { "RetinaAll", (1, retinaToggles.Length) },
            { "Mácula", (3, 4) },
            { "DiscoOptico", (6, retinaToggles.Length) },
            { "EstructurasTransparentesAll", (1, estructurasTransparentesToggles.Length) }
        };
    }

    private void InitializeModelGroups()
    {
        modelGroups = new Dictionary<string, GameObject[]>
        {
            { "TunicaFibrosa", tunicaFibrosaModels },
            { "TunicaVascular", tunicaVascularModels },
            { "Retina", retinaModels },
            { "EstructurasTransparentes", estructurasTransparentesModels }
        };
    }

    private void InitializeModelRanges()
    {
        modelRanges = new Dictionary<string, (int start, int end)>
        {
            { "TunicaFibrosaAll", (0, tunicaFibrosaModels.Length) },
            { "MusculosOculares", (4, tunicaFibrosaModels.Length) },
            { "TunicaVascularAll", (0, tunicaVascularModels.Length) },
            { "CuerpoCiliar", (3, 5) },
            { "RetinaAll", (0, retinaModels.Length) },
            { "Mácula", (3, 4) },
            { "DiscoOptico", (6, retinaModels.Length) },
            { "EstructurasTransparentesAll", (0, estructurasTransparentesModels.Length) }
        };
    }

    public void SetToggles(string groupName, string indexKey, bool value)
    {
        if (toggleGroups.TryGetValue(groupName, out Toggle[] toggles) &&
            toggleRanges.TryGetValue(indexKey, out (int start, int end) toggleRange) &&
            modelGroups.TryGetValue(groupName, out GameObject[] models) &&
            modelRanges.TryGetValue(indexKey, out (int start, int end) modelRange))
        {
            if (value)
            {
                DeactivateOtherGroups(indexKey);
            }

            SetGroupToggles(toggles, toggleRange.start, toggleRange.end, value);
            SetGroupModels(models, modelRange.start, modelRange.end, value);
        }
        else
        {
            Debug.LogWarning($"Invalid group name or index key: {groupName}, {indexKey}");
        }
    }

    private void DeactivateOtherGroups(string activeIndexKey)
    {
        if (activeIndexKey == "TunicaFibrosaAll")
        {
            DeactivateToggles("TunicaVascular", "TunicaVascularAll");
            DeactivateToggles("Retina", "RetinaAll");
            DeactivateToggles("EstructurasTransparentes", "EstructurasTransparentesAll");
        }
        else if (activeIndexKey == "TunicaVascularAll")
        {
            DeactivateToggles("TunicaFibrosa", "TunicaFibrosaAll");
            DeactivateToggles("Retina", "RetinaAll");
            DeactivateToggles("EstructurasTransparentes", "EstructurasTransparentesAll");
        }
        else if (activeIndexKey == "RetinaAll")
        {
            DeactivateToggles("TunicaFibrosa", "TunicaFibrosaAll");
            DeactivateToggles("TunicaVascular", "TunicaVascularAll");
            DeactivateToggles("EstructurasTransparentes", "EstructurasTransparentesAll");
        }else if (activeIndexKey == "EstructurasTransparentesAll")
        {
            DeactivateToggles("TunicaFibrosa", "TunicaFibrosaAll");
            DeactivateToggles("TunicaVascular", "TunicaVascularAll");
            DeactivateToggles("Retina", "RetinaAll");
        }
    }

    private void SetGroupToggles(Toggle[] toggles, int start, int end, bool value)
    {
        for (int i = start; i < end; i++)
        {
            toggles[i].isOn = value;
            toggles[i].gameObject.SetActive(value);
        }
    }

    private void SetGroupModels(GameObject[] models, int start, int end, bool value)
    {
        for (int i = start; i < end; i++)
        {
            models[i].SetActive(value);
        }
    }

    private void DeactivateToggles(string groupName, string indexKey)
    {
        if (toggleGroups.TryGetValue(groupName, out Toggle[] toggles) &&
            toggleRanges.TryGetValue(indexKey, out (int start, int end) indices))
        {
            toggles[0].isOn = false;
            SetGroupToggles(toggles, indices.start, indices.end, false);
            SetGroupModels(modelGroups[groupName], 0, modelGroups[groupName].Length, false);
        }
    }

    public void SetTunicaFibrosaToggles(bool value) => SetToggles("TunicaFibrosa", "TunicaFibrosaAll", value);
    public void SetMusculosOculares(bool value) => SetToggles("TunicaFibrosa", "MusculosOculares", value);
    public void SetTunicaVascularToggles(bool value) => SetToggles("TunicaVascular", "TunicaVascularAll", value);
    public void SetCuerpoCiliar(bool value) => SetToggles("TunicaVascular", "CuerpoCiliar", value);
    public void SetRetinaToggles(bool value) => SetToggles("Retina", "RetinaAll", value);
    public void SetMacula(bool value) => SetToggles("Retina", "Mácula", value);
    public void SetDiscoOptico(bool value) => SetToggles("Retina", "DiscoOptico", value);
    public void SetEstructurasTransparentes(bool value) => SetToggles("EstructurasTransparentes", "EstructurasTransparentesAll", value);

    public void EditEncyclopediaHeader(string text)
    {
        textHeaderEnciclopedia.text = text;
    }

    public void EditEncyclopediaHeight(int height)
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(746, height);
    }
}
