using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EnciclopediaManagerTest : MonoBehaviour
{
    [SerializeField] private TMP_Text textHeaderEnciclopedia;
    [SerializeField] private Toggle[] tunicaFibrosaToggles;
    [SerializeField] private Toggle[] tunicaVascularToggles;
    [SerializeField] private Toggle[] retinaToggles;

    [SerializeField] private GameObject[] tunicaFibrosaModels;
    [SerializeField] private GameObject[] tunicaVascularModels;
    [SerializeField] private GameObject[] retinaModels;

    private Dictionary<string, Toggle[]> toggleGroups;
    private Dictionary<string, (int start, int end)> toggleRanges;
    private Dictionary<string, GameObject[]> modelGroups;
    private Dictionary<string, (int start, int end)> modelRanges;

    private void Awake()
    {
        InitializeToggleGroups();
        InitializeToggleRanges();
        InitializeModelGroups();
        InitializeModelRanges();
    }

    private void InitializeToggleGroups()
    {
        toggleGroups = new Dictionary<string, Toggle[]>
        {
            { "TunicaFibrosa", tunicaFibrosaToggles },
            { "TunicaVascular", tunicaVascularToggles },
            { "Retina", retinaToggles }
        };
    }

    private void InitializeToggleRanges()
    {
        toggleRanges = new Dictionary<string, (int start, int end)>
        {
            { "TunicaFibrosaAll", (1, tunicaFibrosaToggles.Length) },
            { "Córnea", (1, 1) },
            { "Esclera", (2, 2) },
            { "MusculosOculares", (3, 3) },
            { "HuesoOblicuoSuperior", (4,4)},
            { "NervioÓptico", (5, 5)},
            { "TunicaVascularAll", (1, tunicaVascularToggles.Length) },
            { "Iris", (1, 1) },
            { "ProcesosCiliaresTunicaVascular", (2, 2) },
            { "Coroides", (3, 3) },
            { "RetinaAll", (1, retinaToggles.Length) },
            { "Retina", (1, 1) },
            { "ProcesosCiliaresRetina", (2, 2) },
            { "AteriaCentral", (3, 3) },
            { "VenaCentral", (4, 4) }
        };
    }

    private void InitializeModelGroups()
    {
        modelGroups = new Dictionary<string, GameObject[]>
        {
            { "TunicaFibrosa", tunicaFibrosaModels },
            { "TunicaVascular", tunicaVascularModels },
            { "Retina", retinaModels }
        };
    }

    private void InitializeModelRanges()
    {
        modelRanges = new Dictionary<string, (int start, int end)>
        {
            { "TunicaFibrosaAll", (0, tunicaFibrosaModels.Length) },
            { "Córnea", (0, 1) },
            { "Esclera", (1, 2) },
            { "MusculosOculares", (2, 3) },
            { "HuesoOblicuoSuperior", (3,4)},
            { "NervioÓptico", (4, 5)},
            { "TunicaVascularAll", (0, tunicaVascularModels.Length) },
            { "Iris", (0, 1) },
            { "ProcesosCiliaresTunicaVascular", (1, 2) },
            { "Coroides", (2, 3) },
            { "RetinaAll", (0, retinaModels.Length) },
            { "Retina", (0, 1) },
            { "ProcesosCiliaresRetina", (1, 2) },
            { "AteriaCentral", (2, 3) },
            { "VenaCentral", (3, 4) }
        };
    }

    public void SetToggles(string groupName, string indexKey, bool value)
    {
        if (toggleGroups.TryGetValue(groupName, out Toggle[] toggles) &&
            toggleRanges.TryGetValue(indexKey, out (int start, int end) toggleRange) &&
            modelGroups.TryGetValue(groupName, out GameObject[] models) &&
            modelRanges.TryGetValue(indexKey, out (int start, int end) modelRange))
        {
            if (value)
            {
                DeactivateOtherGroups(indexKey);
            }

            SetGroupToggles(toggles, toggleRange.start, toggleRange.end, value);
            SetGroupModels(models, modelRange.start, modelRange.end, value);
        }
        else
        {
            Debug.LogWarning($"Invalid group name or index key: {groupName}, {indexKey}");
        }
    }

    private void DeactivateOtherGroups(string activeIndexKey)
    {
        if (activeIndexKey == "TunicaFibrosaAll")
        {
            DeactivateToggles("TunicaVascular", "TunicaVascularAll");
            DeactivateToggles("Retina", "RetinaAll");
        }
        else if (activeIndexKey == "TunicaVascularAll")
        {
            DeactivateToggles("TunicaFibrosa", "TunicaFibrosaAll");
            DeactivateToggles("Retina", "RetinaAll");
        }
        else if (activeIndexKey == "RetinaAll")
        {
            DeactivateToggles("TunicaFibrosa", "TunicaFibrosaAll");
            DeactivateToggles("TunicaVascular", "TunicaVascularAll");
        }
    }

    private void SetGroupToggles(Toggle[] toggles, int start, int end, bool value)
    {
        for (int i = start; i < end; i++)
        {
            toggles[i].isOn = value;
            toggles[i].gameObject.SetActive(value);
        }
    }

    private void SetGroupModels(GameObject[] models, int start, int end, bool value)
    {
        for (int i = start; i < end; i++)
        {
            models[i].SetActive(value);
        }
    }

    private void DeactivateToggles(string groupName, string indexKey)
    {
        if (toggleGroups.TryGetValue(groupName, out Toggle[] toggles) &&
            toggleRanges.TryGetValue(indexKey, out (int start, int end) indices))
        {
            toggles[0].isOn = false;
            SetGroupToggles(toggles, indices.start, indices.end, false);
            SetGroupModels(modelGroups[groupName], 0, modelGroups[groupName].Length, false);
        }
    }

    public void SetTunicaFibrosaToggles(bool value) => SetToggles("TunicaFibrosa", "TunicaFibrosaAll", value);
    public void SetTunicaVascularToggles(bool value) => SetToggles("TunicaVascular", "TunicaVascularAll", value);
    public void SetRetinaToggles(bool value) => SetToggles("Retina", "RetinaAll", value);
    public void SetCórneaToggles(bool value) => SetToggles("TunicaFibrosa", "Córnea", value);
    public void SetEscleraToggles(bool value) => SetToggles("TunicaFibrosa", "Esclera", value);
    public void SetMusculosOcularesToggles(bool value) => SetToggles("TunicaFibrosa", "MusculosOculares", value);
    public void SetHuesoOblicuoSuperiorToggles(bool value) => SetToggles("TunicaFibrosa", "HuesoOblicuoSuperior", value);
    public void SetNervioÓpticoToggles(bool value) => SetToggles("TunicaFibrosa", "NervioÓptico", value);
    public void SetIrisToggles(bool value) => SetToggles("TunicaVascular", "Iris", value);
    public void SetProcesosCiliaresTunicaVascularToggles(bool value) => SetToggles("TunicaVascular", "ProcesosCiliaresTunicaVascular", value);
    public void SetCoroidesToggles(bool value) => SetToggles("TunicaVascular", "Coroides", value);
    public void SetRetinaRetinaToggles(bool value) => SetToggles("Retina", "Retina", value);
    public void SetProcesosCiliaresToggles(bool value) => SetToggles("Retina", "ProcesosCiliaresRetina", value);
    public void SetAteriaCentralToggles(bool value) => SetToggles("Retina", "AteriaCentral", value);
    public void SetVenaCentralToggles(bool value) => SetToggles("Retina", "VenaCentral", value);

    public void EditEncyclopediaHeader(string text)
    {
        textHeaderEnciclopedia.text = text;
    }

    public void EditEncyclopediaHeight(int height)
    {
        GetComponent<RectTransform>().sizeDelta = new Vector2(746, height);
    }
}
