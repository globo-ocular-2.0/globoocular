 using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManagger : MonoBehaviour
{
    [SerializeField] private GameObject[] canvas;
    public void WelcomeCanvas(bool active)
    {
        canvas[0].gameObject.SetActive(active);
    }

    public void MainCanvas(bool active)
    {
        canvas[1].gameObject.SetActive(active);
    }

    public void ModalCanvas(bool active)
    {
        canvas[2].gameObject.SetActive(active);
    }
    public void DisableCanvas()
    {
        foreach (var item in canvas)
        {
            item.SetActive(false);
        }
    }

}
