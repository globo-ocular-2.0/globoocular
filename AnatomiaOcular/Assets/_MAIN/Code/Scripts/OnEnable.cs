using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnable : MonoBehaviour
{
    bool _onEnable = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Enable()
    {
        gameObject.SetActive(_onEnable);
        _onEnable = !_onEnable;
    }
}
