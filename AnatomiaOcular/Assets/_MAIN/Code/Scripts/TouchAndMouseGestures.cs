using UnityEngine;
using UnityEngine.EventSystems; // Necesario para detectar UI

public class TouchAndMouseGestures : MonoBehaviour
{
    [Header("Sensibilidad")]
    public float moveSensitivity = 0.1f;
    public float scaleSensitivity = 0.01f;
    public float rotationSensitivity = 0.1f;

    [Header("Límites de traslación")]
    public Vector3 minPosition = new Vector3(-5f, -5f, -5f);
    public Vector3 maxPosition = new Vector3(5f, 5f, 5f);

    [Header("Límites de rotación")]
    public bool limitRotationX = false;
    public bool limitRotationY = false;
    public bool limitRotationZ = false;
    public float minRotationX = -45f;
    public float maxRotationX = 45f;
    public float minRotationY = -45f;
    public float maxRotationY = 45f;
    public float minRotationZ = -45f;
    public float maxRotationZ = 45f;

    private Vector3 initialScale;
    private float initialTouchDistance;
    private Vector3 lastMousePosition;

    void Start()
    {
        initialScale = transform.localScale;
    }

    void Update()
    {
        if (Input.touchSupported && Input.touchCount > 0)
        {
            HandleTouch();
        }
        else
        {
            HandleMouse();
        }
    }

    void HandleTouch()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            // Verificar si el toque está sobre un UI
            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
            {
                return; // Si está tocando UI, no mover
            }

            if (touch.phase == TouchPhase.Moved)
            {
                // Movimiento con un dedo
                Vector3 deltaPosition = new Vector3(touch.deltaPosition.x, touch.deltaPosition.y, 0) * moveSensitivity * Time.deltaTime;
                Vector3 newPosition = transform.position + deltaPosition;
                transform.position = ClampPosition(newPosition);
            }
        }
        else if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Escalar y rotar solo si ninguno de los toques está en la UI
            if (EventSystem.current.IsPointerOverGameObject(touchZero.fingerId) || EventSystem.current.IsPointerOverGameObject(touchOne.fingerId))
            {
                return; // Si algún toque está sobre UI, no escalar ni rotar
            }

            // Escalar
            float currentTouchDistance = Vector2.Distance(touchZero.position, touchOne.position);
            if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
            {
                initialTouchDistance = currentTouchDistance;
            }
            else if (touchZero.phase == TouchPhase.Moved || touchOne.phase == TouchPhase.Moved)
            {
                float scaleFactor = (currentTouchDistance - initialTouchDistance) * scaleSensitivity;
                transform.localScale += new Vector3(scaleFactor, scaleFactor, scaleFactor);
                initialTouchDistance = currentTouchDistance;
            }

            // Rotar
            Vector2 prevTouchZeroPos = touchZero.position - touchZero.deltaPosition;
            Vector2 prevTouchOnePos = touchOne.position - touchOne.deltaPosition;
            float prevAngle = Vector2.SignedAngle(prevTouchZeroPos - prevTouchOnePos, Vector2.right);
            float currentAngle = Vector2.SignedAngle(touchZero.position - touchOne.position, Vector2.right);
            float angleDelta = currentAngle - prevAngle;

            RotateObject(angleDelta * rotationSensitivity);
        }
    }

    void HandleMouse()
    {
        // Verificar si el clic del mouse está sobre UI
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return; // Si está tocando UI, no mover
        }

        if (Input.GetMouseButton(0)) // Botón izquierdo del mouse para movimiento
        {
            Vector3 deltaPosition = (Input.mousePosition - lastMousePosition) * moveSensitivity * Time.deltaTime;
            deltaPosition.z = 0;
            Vector3 newPosition = transform.position + deltaPosition;
            transform.position = ClampPosition(newPosition);
        }
        else if (Input.GetMouseButton(1)) // Botón derecho del mouse para rotación
        {
            Vector3 deltaRotation = Input.mousePosition - lastMousePosition;
            float rotationZ = deltaRotation.x * rotationSensitivity;
            RotateObject(rotationZ);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") != 0f) // Rueda del mouse para escalado
        {
            float scaleFactor = Input.GetAxis("Mouse ScrollWheel") * scaleSensitivity;
            transform.localScale += new Vector3(scaleFactor, scaleFactor, scaleFactor);
        }

        lastMousePosition = Input.mousePosition;
    }

    private Vector3 ClampPosition(Vector3 position)
    {
        position.x = Mathf.Clamp(position.x, minPosition.x, maxPosition.x);
        position.y = Mathf.Clamp(position.y, minPosition.y, maxPosition.y);
        position.z = Mathf.Clamp(position.z, minPosition.z, maxPosition.z);
        return position;
    }

    private void RotateObject(float rotationZ)
    {
        Vector3 currentRotation = transform.eulerAngles;

        // Aplicamos rotación en el eje Z (u otro si es necesario)
        currentRotation.z += rotationZ;

        if (limitRotationX)
        {
            currentRotation.x = Mathf.Clamp(currentRotation.x, minRotationX, maxRotationX);
        }
        if (limitRotationY)
        {
            currentRotation.y = Mathf.Clamp(currentRotation.y, minRotationY, maxRotationY);
        }
        if (limitRotationZ)
        {
            currentRotation.z = Mathf.Clamp(currentRotation.z, minRotationZ, maxRotationZ);
        }

        transform.eulerAngles = currentRotation;
    }
}
