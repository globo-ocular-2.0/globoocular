using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeSettings : MonoBehaviour
{
    [SerializeField] private GameObject[] preSetEyesPosition;
    // Start is called before the first frame update

    public void ResetTransformValues(){
        gameObject.transform.localPosition = new Vector3(0, 0, 0);
        gameObject.transform.localRotation = Quaternion.Euler(0, 180, 0);
        gameObject.transform.localScale = new Vector3(1, 1, 1);

    }

    public void SetEyePosition(string positionMode)
    {
        switch(positionMode){
            case "Position1":
                gameObject.transform.parent = preSetEyesPosition[0].transform;
                gameObject.transform.localPosition = new Vector3(0, 0, 0);
                break;
            case "Position2":         
                gameObject.transform.parent = preSetEyesPosition[1].transform;
                gameObject.transform.localPosition = new Vector3(0, 0, 0);
                break;
            case "Position3":        
                gameObject.transform.parent = preSetEyesPosition[2].transform;
                gameObject.transform.localPosition = new Vector3(0, 0, 0);
                break;
        }

    }
}
