using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIVistasMovimientosOculares : MonoBehaviour
{
    [Header("Buttons Images")]
    [SerializeField] private Image btnMovimientosExtraOculares;
    [SerializeField] private Image btnMovimientosIntraOculares;
    [SerializeField] private Image btnControladorExtraOcular;
    [SerializeField] private Image btnControladorIntraOcular;

    [Header("Sprites Buttons")]
    [SerializeField] private Sprite sprMovimientosExtraOcularesEnable;
    [SerializeField] private Sprite sprMovimientosExtraOcularesDisable;
    [SerializeField] private Sprite sprMovimientosIntraOcularesEnable;
    [SerializeField] private Sprite sprMovimientosIntraOcularesDisable;
    [SerializeField] private Sprite sprControladorH;
    [SerializeField] private Sprite sprControladorPlus;
    [Header("Labels 3D Completo")]
    [SerializeField] private GameObject dextroSupraversionC;
    [SerializeField] private GameObject dextroInfraversionC;
    [SerializeField] private GameObject levosupraversionC;
    [SerializeField] private GameObject levoinfraversionC;
    [SerializeField] private GameObject supraversionC;
    [SerializeField] private GameObject infraversionC;
    [SerializeField] private GameObject abduccionC;
    [SerializeField] private GameObject aduccionC;
    [SerializeField] private GameObject pupilaDilatadaC;
    [SerializeField] private GameObject pupilaContraidaC;
    [SerializeField] private GameObject aplanarC;
    [SerializeField] private GameObject abombaC;
    [Header("Labels 3D Sagital")]
    [SerializeField] private GameObject dextroSupraversionS;
    [SerializeField] private GameObject dextroInfraversionS;
    [SerializeField] private GameObject levosupraversionS;
    [SerializeField] private GameObject levoinfraversionS;
    [SerializeField] private GameObject supraversionS;
    [SerializeField] private GameObject infraversionS;
    [SerializeField] private GameObject abduccionS;
    [SerializeField] private GameObject aduccionS;
    [SerializeField] private GameObject pupilaDilatadaS;
    [SerializeField] private GameObject pupilaContraidaS;
    [SerializeField] private GameObject aplanarS;
    [SerializeField] private GameObject abombaS;
    [Header("Labels 3D overlay")]
    [SerializeField] private GameObject objLabelMusculos;
    [SerializeField] private GameObject objLabelMovimientos;
    [SerializeField] private TMP_Text textLabelMusculos;
    [SerializeField] private TMP_Text textLabelMovimientos;
    [Header("Panels")]
    [SerializeField] private GameObject panelExtraOcular;
    [SerializeField] private GameObject panelIntraOcular;
    [SerializeField] private GameObject panelH;
    [SerializeField] private GameObject panelPlus;
    [SerializeField] private GameObject controladorIris;
    [SerializeField] private GameObject controladorCristalino;

    [Header("Scripts")]
    [SerializeField] private VistaOcularController vistaOcularController;
    // Start is called before the first frame update
    void Start()
    {
        SwitchSprite("ExtraOculares");
        SwitchSpriteControlador(true);
        SwitchSpriteIntraControlador(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SwitchSprite(string movimiento)
    {
        switch (movimiento)
        {
            case "ExtraOculares":
                btnMovimientosExtraOculares.sprite = sprMovimientosExtraOcularesEnable;
                btnMovimientosIntraOculares.sprite = sprMovimientosIntraOcularesDisable;
                panelExtraOcular.SetActive(true);
                panelIntraOcular.SetActive(false);
                break;
            case "IntraOculares":
                btnMovimientosExtraOculares.sprite = sprMovimientosExtraOcularesDisable;
                btnMovimientosIntraOculares.sprite = sprMovimientosIntraOcularesEnable;
                panelExtraOcular.SetActive(false);
                panelIntraOcular.SetActive(true);
                break;
        }
    }

    public void SwitchSpriteControlador(bool controlador)
    {
        if (controlador)
        {
            btnControladorExtraOcular.sprite = sprControladorPlus;
            panelH.SetActive(true);
            panelPlus.SetActive(false);
        }
        else
        {
            btnControladorExtraOcular.sprite = sprControladorH;
            panelH.SetActive(false);
            panelPlus.SetActive(true);
        }
    }
        public void SwitchSpriteIntraControlador(bool controlador)
    {
        if (controlador)
        {
            btnControladorIntraOcular.sprite = sprControladorPlus;
            controladorIris.SetActive(true);
            controladorCristalino.SetActive(false);
        }
        else
        {
            btnControladorIntraOcular.sprite = sprControladorH;
            controladorIris.SetActive(false);
            controladorCristalino.SetActive(true);
        }
    }

    public void AnimLabelMovimientos(string value)
    {
        SetActiveLabelMovimientosExtraOculares(false);
        switch(value)
        {
            case "dextroSupraversion":
                SetActiveLabelMovimientosExtraOculares(true);
                textLabelMovimientos.text = "Movimiento: afuera y arriba.";
                textLabelMusculos.text = "Músculo: recto superior.";
            break;
            case "dextroInfraversion":
                SetActiveLabelMovimientosExtraOculares(true);
                textLabelMovimientos.text = "Movimiento: afuera y abajo.";
                textLabelMusculos.text = "Músculo: recto inferior.";
            break;
            case "levosupraversion":
                SetActiveLabelMovimientosExtraOculares(true);
                textLabelMovimientos.text = "Movimiento: adentro y arriba.";
                textLabelMusculos.text = "Músculo: oblicuo superior.";
            break;
            case "levoinfraversion":
                SetActiveLabelMovimientosExtraOculares(true);
                textLabelMovimientos.text = "Movimiento: adentro y abajo.";
                textLabelMusculos.text = "Músculo: oblicuo inferior.";
            break;
            case "supraversion":
                SetActiveLabelMovimientosExtraOculares(true);
                textLabelMovimientos.text = "Movimiento: arriba.";
                textLabelMusculos.text = "Músculo: recto superior.";
            break;
            case "infraversion":
                SetActiveLabelMovimientosExtraOculares(true);
                textLabelMovimientos.text = "Movimiento: abajo.";
                textLabelMusculos.text = "Músculo: recto inferior.";
            break;
            case "abduccion":
                SetActiveLabelMovimientosExtraOculares(true);
                textLabelMovimientos.text = "Movimiento: afuera.";
                textLabelMusculos.text = "Músculo: recto lateral.";
            break;
            case "aduccion":
                SetActiveLabelMovimientosExtraOculares(true);
                textLabelMovimientos.text = "Movimiento: adentro.";
                textLabelMusculos.text = "Músculo: recto medial.";
            break;
            case "pupilaDilatada":
                SetActiveLabelMovimientosIntraOculares();
                textLabelMovimientos.text = "Pupila dilatada, iridodilatación o midriasis.";
            break;
            case "pupilaContraida":
                SetActiveLabelMovimientosIntraOculares();
                textLabelMovimientos.text = "Pupila contraída, iridoconstricción o miosis.";
            break;
            case "aplanar":
                SetActiveLabelMovimientosIntraOculares();
                textLabelMovimientos.text = "Movimiento: cristalino aplanado.";
            break;
            case "abombado":
                SetActiveLabelMovimientosIntraOculares();
                textLabelMovimientos.text = "Movimiento: cristalino abombado.";
            break;

        }
    }
    public void SetActiveLabelMovimientosExtraOculares(bool value)
    {
        objLabelMovimientos.SetActive(value);
        objLabelMusculos.SetActive(value);
    }
    public void SetActiveLabelMovimientosIntraOculares()
    {
        objLabelMovimientos.SetActive(true);
        objLabelMusculos.SetActive(false);
    }
   
}
    
    
