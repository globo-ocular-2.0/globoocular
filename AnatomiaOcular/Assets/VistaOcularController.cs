using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Unity.VisualScripting;

public class VistaOcularController : MonoBehaviour
{

    // Botones para seleccionar las túnicas
    [Header("Modelos completos")]
    public GameObject modelGloboOcular;
    public GameObject modelGloboSagital;
    [Header("Modelos Globo Ocular")]
    public GameObject arteriaP;
    public GameObject camaraAcuosaAnterior;
    public GameObject camaraAcuosaPosterior;
    public GameObject CamaraVitrea;
    public GameObject cornea;
    public GameObject corneaBrillo;
    public GameObject coroides;
    public GameObject cristalino;
    public GameObject esclera;
    public GameObject iris;
    public GameObject procesosCiliar;
    public GameObject retina;
    public GameObject venaVorticosa;
    public GameObject femaleSkull;
    public GameObject huesoOblicuoSuperior;
    public GameObject musculoRectoSuperior;
    public GameObject musculoOblicuoInferior;
    public GameObject musculoRectoInferior;
    public GameObject musculoRectoMedial;
    public GameObject musculoRectoLateral;
    public GameObject musculoOblicuoSuperior;
    public GameObject nervioOptico;
    [Header("Modelos Globo Sagital")]
    public GameObject arteriasSagital;
    public GameObject camaraAcuosaSagitalAnterior;
    public GameObject camaraAcuosaSagitalPosterior;
    public GameObject canalHialoideoSagital;
    public GameObject coroidesSagital;
    public GameObject corneaSagital;
    public GameObject corneaBrilloSagital;
    public GameObject corteTransversalSagital;
    public GameObject cristalinoSagital;
    public GameObject escleraSagital;
    public GameObject irisSagital;
    public GameObject retinaSagital;
    public GameObject prosesosCiliarSagital;
    public GameObject venaVorticosaSagital;
    public GameObject arteriaCentralSagital;
    public GameObject venasSagital;
    public GameObject nervioOpticoExternoSagital;
    public GameObject nervioOpticoInternoSagital;




    [Header("Scripts")]
    [SerializeField] private UIHeader uiHeader;
    [SerializeField] private UIVistaOcular uiVistaOcular;
    [SerializeField] private UIVistasTunicaOcular uiVistasTunicaOcular;
    [SerializeField] private UIVistasMovimientosOculares uiVistasMovimientosOculares;


    void Start()
    {
       VistaGloboOcular("GloboOcular");
       //VistaTunicasDelGloboOcular("TunicaFibrosa");
    }
    public void VistaGloboOcular(string vista)
    {
        switch (vista)
        {
            case "GloboOcular":
                modelGloboOcular.SetActive(true);
                modelGloboSagital.SetActive(false);
                uiVistaOcular.SwitchSpriteVistaOcular("GloboOcular");
                uiVistasTunicaOcular.SpesialConponentInteractablesGloboOcular(true);
                uiVistasTunicaOcular.SpesialConponentInteractablesGloboSagital(false);
                break;
            case "GloboSagital":
                modelGloboOcular.SetActive(false);
                modelGloboSagital.SetActive(true);
                uiVistaOcular.SwitchSpriteVistaOcular("GloboSagital");
                uiVistasTunicaOcular.SpesialConponentInteractablesGloboOcular(false);
                uiVistasTunicaOcular.SpesialConponentInteractablesGloboSagital(true);

                break;
        }
    }
    public void VistaTunicasDelGloboOcular(string vista)
    {
        ShowAllModelos(false);
        switch (vista)
        {
            case "TunicaFibrosa":
            //Globo Ocular
                arteriaP.SetActive(false);
                camaraAcuosaAnterior.SetActive(false);
                camaraAcuosaPosterior.SetActive(false);
                CamaraVitrea.SetActive(false);
                cornea.SetActive(true);
                corneaBrillo.SetActive(false);
                coroides.SetActive(false);
                cristalino.SetActive(false);
                esclera.SetActive(true);
                iris.SetActive(false);
                procesosCiliar.SetActive(false);
                retina.SetActive(false);
                venaVorticosa.SetActive(false);
                femaleSkull.SetActive(false);
                huesoOblicuoSuperior.SetActive(false);
                musculoRectoSuperior.SetActive(true);
                musculoOblicuoInferior.SetActive(true);
                musculoOblicuoSuperior.SetActive(true);
                musculoRectoInferior.SetActive(true);
                musculoRectoMedial.SetActive(true);
                musculoRectoLateral.SetActive(true);

                nervioOptico.SetActive(false);
                //Globo Sagital
                arteriasSagital.SetActive(false);
                camaraAcuosaSagitalAnterior.SetActive(false);
                camaraAcuosaSagitalPosterior.SetActive(false);
                canalHialoideoSagital.SetActive(false);
                coroidesSagital.SetActive(false);
                corneaSagital.SetActive(true);
                corneaBrilloSagital.SetActive(false);
                corteTransversalSagital.SetActive(false);
                cristalinoSagital.SetActive(false);
                escleraSagital.SetActive(true);
                irisSagital.SetActive(false);
                prosesosCiliarSagital.SetActive(false);
                retinaSagital.SetActive(false);
                venaVorticosaSagital.SetActive(false);
                arteriaCentralSagital.SetActive(false);
                nervioOpticoExternoSagital.SetActive(false);
                nervioOpticoInternoSagital.SetActive(false);
                venasSagital.SetActive(false);


                //Sprites
                uiVistaOcular.SwitchSpriteVistaOcular("TunicaFibrosa");
                uiVistasTunicaOcular.SwitchSprite("TunicaFibrosa");
                break;
            case "TunicaVascular":
                arteriaP.SetActive(false);
                camaraAcuosaAnterior.SetActive(false);
                camaraAcuosaPosterior.SetActive(false);
                CamaraVitrea.SetActive(false);
                cornea.SetActive(false);
                corneaBrillo.SetActive(false);
                coroides.SetActive(true);
                cristalino.SetActive(false);
                esclera.SetActive(false);
                iris.SetActive(true);
                procesosCiliar.SetActive(true);
                retina.SetActive(false);
                venaVorticosa.SetActive(false);
                femaleSkull.SetActive(false);
                huesoOblicuoSuperior.SetActive(false);
                musculoRectoSuperior.SetActive(false);
                musculoOblicuoInferior.SetActive(false);
                musculoOblicuoSuperior.SetActive(false);
                musculoRectoInferior.SetActive(false);
                musculoRectoMedial.SetActive(false);
                musculoRectoLateral.SetActive(false);
                nervioOptico.SetActive(false);
                 //Globo Sagital
                arteriasSagital.SetActive(false);
                camaraAcuosaSagitalAnterior.SetActive(false);
                camaraAcuosaSagitalPosterior.SetActive(false);
                canalHialoideoSagital.SetActive(false);
                coroidesSagital.SetActive(true);
                prosesosCiliarSagital.SetActive(true);
                corneaSagital.SetActive(false);
                corneaBrilloSagital.SetActive(false);
                corteTransversalSagital.SetActive(true);
                cristalinoSagital.SetActive(false);
                escleraSagital.SetActive(false);
                irisSagital.SetActive(true);
                retinaSagital.SetActive(false);
                venaVorticosaSagital.SetActive(false);
                arteriaCentralSagital.SetActive(false);
                nervioOpticoExternoSagital.SetActive(false);
                nervioOpticoInternoSagital.SetActive(false);
                venasSagital.SetActive(false);
                //Sprites
                uiVistaOcular.SwitchSpriteVistaOcular("TunicaVascular");
                uiVistasTunicaOcular.SwitchSprite("TunicaVascular");
                
                break;
            case "Retina": 
                arteriaP.SetActive(false);
                camaraAcuosaAnterior.SetActive(false);
                camaraAcuosaPosterior.SetActive(false);
                CamaraVitrea.SetActive(false);
                cornea.SetActive(false);
                corneaBrillo.SetActive(false);
                coroides.SetActive(false);
                cristalino.SetActive(false);
                esclera.SetActive(false);
                iris.SetActive(false);
                procesosCiliar.SetActive(true);
                retina.SetActive(true);
                venaVorticosa.SetActive(false);
                femaleSkull.SetActive(false);
                huesoOblicuoSuperior.SetActive(false);
                musculoRectoSuperior.SetActive(false);
                musculoOblicuoInferior.SetActive(false);
                musculoOblicuoSuperior.SetActive(false);
                musculoRectoInferior.SetActive(false);
                musculoRectoMedial.SetActive(false);
                musculoRectoLateral.SetActive(false);
                nervioOptico.SetActive(true);
                 //Globo Sagital
                arteriasSagital.SetActive(false);
                camaraAcuosaSagitalAnterior.SetActive(false);
                camaraAcuosaSagitalPosterior.SetActive(false);
                canalHialoideoSagital.SetActive(false);
                coroidesSagital.SetActive(false);
                corneaSagital.SetActive(false);
                corneaBrilloSagital.SetActive(false);
                corteTransversalSagital.SetActive(false);
                cristalinoSagital.SetActive(false);
                escleraSagital.SetActive(false);
                irisSagital.SetActive(false);
                retinaSagital.SetActive(true);
                prosesosCiliarSagital.SetActive(true);
                venaVorticosaSagital.SetActive(false);
                arteriaCentralSagital.SetActive(true);
                nervioOpticoExternoSagital.SetActive(true);
                nervioOpticoInternoSagital.SetActive(true);
                venasSagital.SetActive(true);
                //Sprites
                uiVistaOcular.SwitchSpriteVistaOcular("Retina");
                uiVistasTunicaOcular.SwitchSprite("Retina");
                break;
            case "MediosTransparentes":
                arteriaP.SetActive(false);
                camaraAcuosaAnterior.SetActive(true);
                camaraAcuosaPosterior.SetActive(true);
                CamaraVitrea.SetActive(true);
                cornea.SetActive(true);
                corneaBrillo.SetActive(true);
                coroides.SetActive(false);
                cristalino.SetActive(true);
                esclera.SetActive(false);
                iris.SetActive(false);
                procesosCiliar.SetActive(false);
                retina.SetActive(false);
                venaVorticosa.SetActive(false);
                femaleSkull.SetActive(false);
                huesoOblicuoSuperior.SetActive(false);
                musculoRectoSuperior.SetActive(false);
                musculoOblicuoInferior.SetActive(false);
                musculoOblicuoSuperior.SetActive(false);
                musculoRectoInferior.SetActive(false);
                musculoRectoMedial.SetActive(false);
                musculoRectoLateral.SetActive(false);
                nervioOptico.SetActive(false);
                 //Globo Sagital
                arteriasSagital.SetActive(false);
                camaraAcuosaSagitalAnterior.SetActive(true);
                camaraAcuosaSagitalPosterior.SetActive(true);
                canalHialoideoSagital.SetActive(true);
                coroidesSagital.SetActive(false);
                corneaSagital.SetActive(false);
                corneaBrilloSagital.SetActive(false);
                corteTransversalSagital.SetActive(false);
                cristalinoSagital.SetActive(true);
                prosesosCiliarSagital.SetActive(false);
                escleraSagital.SetActive(false);
                irisSagital.SetActive(false);
                retinaSagital.SetActive(false);
                venaVorticosaSagital.SetActive(false);
                arteriaCentralSagital.SetActive(false);
                nervioOpticoExternoSagital.SetActive(false);
                nervioOpticoInternoSagital.SetActive(false);
                venasSagital.SetActive(false);
                //Sprites
                uiVistaOcular.SwitchSpriteVistaOcular("MediosTransparentes");
                uiVistasTunicaOcular.SwitchSprite("MediosTransparentes");
                break;

        }
    }
    public void VistaMovimientosOculares(string vista)
    {
        ShowAllModelos(false);
        switch (vista)
        {
            case "ExtraOculares":
                VistaGloboOcular("GloboOcular");
                uiHeader.InteractableButtons(false);
                //Globo Ocular
                arteriaP.SetActive(true);
                camaraAcuosaAnterior.SetActive(false);
                camaraAcuosaPosterior.SetActive(false);
                CamaraVitrea.SetActive(false);
                cornea.SetActive(true);
                corneaBrillo.SetActive(false);
                coroides.SetActive(false);
                cristalino.SetActive(false);
                esclera.SetActive(true);
                iris.SetActive(true);
                procesosCiliar.SetActive(false);
                retina.SetActive(true);
                venaVorticosa.SetActive(true);
                femaleSkull.SetActive(false);
                huesoOblicuoSuperior.SetActive(true);
                musculoRectoSuperior.SetActive(true);
                musculoOblicuoInferior.SetActive(true);
                musculoRectoInferior.SetActive(true);
                musculoRectoMedial.SetActive(true);
                musculoRectoLateral.SetActive(true);
                musculoOblicuoSuperior.SetActive(true);
                nervioOptico.SetActive(true);

                //Globo Sagital
                arteriasSagital.SetActive(false);
                camaraAcuosaSagitalAnterior.SetActive(false);
                camaraAcuosaSagitalPosterior.SetActive(false);
                canalHialoideoSagital.SetActive(false);
                coroidesSagital.SetActive(false);
                corneaSagital.SetActive(false);
                corneaBrilloSagital.SetActive(false);
                corteTransversalSagital.SetActive(false);
                cristalinoSagital.SetActive(true);
                escleraSagital.SetActive(false);
                irisSagital.SetActive(true);
                retinaSagital.SetActive(true);
                prosesosCiliarSagital.SetActive(false);
                venaVorticosaSagital.SetActive(false);
                arteriaCentralSagital.SetActive(false);
                nervioOpticoExternoSagital.SetActive(false);
                nervioOpticoInternoSagital.SetActive(false);
                venasSagital.SetActive(false);

                uiVistasMovimientosOculares.SwitchSprite("ExtraOculares");
                break;
            case "IntraOculares":
                //VistaGloboOcular("GloboSagital");
                uiHeader.InteractableButtons(true);

                arteriaP.SetActive(false);
                camaraAcuosaAnterior.SetActive(false);
                camaraAcuosaPosterior.SetActive(false);
                CamaraVitrea.SetActive(false);
                cornea.SetActive(false);
                corneaBrillo.SetActive(false);
                coroides.SetActive(false);
                cristalino.SetActive(true);
                esclera.SetActive(false);
                iris.SetActive(true);
                procesosCiliar.SetActive(false);
                retina.SetActive(false);
                venaVorticosa.SetActive(false);
                femaleSkull.SetActive(false);
                huesoOblicuoSuperior.SetActive(false);
                musculoRectoSuperior.SetActive(false);
                musculoOblicuoInferior.SetActive(false);
                musculoRectoInferior.SetActive(false);
                musculoRectoMedial.SetActive(false);
                musculoRectoLateral.SetActive(false);
                musculoOblicuoSuperior.SetActive(false);
                nervioOptico.SetActive(false);

                //Globo Sagital
                arteriasSagital.SetActive(false);
                camaraAcuosaSagitalAnterior.SetActive(false);
                camaraAcuosaSagitalPosterior.SetActive(false);
                canalHialoideoSagital.SetActive(false);
                coroidesSagital.SetActive(false);
                corneaSagital.SetActive(false);
                corneaBrilloSagital.SetActive(false);
                corteTransversalSagital.SetActive(false);
                cristalinoSagital.SetActive(true);
                escleraSagital.SetActive(false);
                irisSagital.SetActive(true);
                retinaSagital.SetActive(false);
                prosesosCiliarSagital.SetActive(false);
                venaVorticosaSagital.SetActive(false);
                arteriaCentralSagital.SetActive(false);
                nervioOpticoExternoSagital.SetActive(false);
                nervioOpticoInternoSagital.SetActive(false);
                venasSagital.SetActive(false);
                
                uiVistasMovimientosOculares.SwitchSprite("IntraOculares");
                break;
        }
    }

    public void ShowAllModelos(bool value)
    {
                //Globo Ocular
        arteriaP.SetActive(value);
        camaraAcuosaAnterior.SetActive(value);
        camaraAcuosaPosterior.SetActive(value);
        CamaraVitrea.SetActive(value);
        cornea.SetActive(value);
        corneaBrillo.SetActive(value);
        coroides.SetActive(value);
        cristalino.SetActive(value);
        esclera.SetActive(value);
        iris.SetActive(value);
        procesosCiliar.SetActive(value);
        retina.SetActive(value);
        venaVorticosa.SetActive(value);
        femaleSkull.SetActive(value);
        huesoOblicuoSuperior.SetActive(value);
        musculoRectoSuperior.SetActive(value);
        musculoOblicuoInferior.SetActive(value);
        musculoRectoInferior.SetActive(value);
        musculoRectoMedial.SetActive(value);
        musculoRectoLateral.SetActive(value);
        musculoOblicuoSuperior.SetActive(value);
        nervioOptico.SetActive(value);
        //Globo Sagital
        arteriasSagital.SetActive(value);
        camaraAcuosaSagitalAnterior.SetActive(value);
        camaraAcuosaSagitalPosterior.SetActive(value);
        canalHialoideoSagital.SetActive(value);
        coroidesSagital.SetActive(value);
        corneaSagital.SetActive(value);
        corneaBrilloSagital.SetActive(value);
        corteTransversalSagital.SetActive(value);
        cristalinoSagital.SetActive(value);
        escleraSagital.SetActive(value);
        irisSagital.SetActive(value);
        retinaSagital.SetActive(value);
        prosesosCiliarSagital.SetActive(value);
        venaVorticosaSagital.SetActive(value);
        arteriaCentralSagital.SetActive(value);
        nervioOpticoExternoSagital.SetActive(value);
        nervioOpticoInternoSagital.SetActive(value);
        venasSagital.SetActive(value);
    }
}
