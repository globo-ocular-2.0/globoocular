using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickCross : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public RectTransform background; // Fondo del joystick (en forma de cruz)
    public RectTransform handle;     // Manejador del joystick

    [Header("Configuración")]
    public float handleRange = 100f; // Rango máximo del joystick
    private Vector2 inputVector;     // Vector que representa la dirección actual

    public JoystickAnimationController animationController; // Controlador de animaciones (opcional)

    public void OnDrag(PointerEventData eventData)
    {
        // Convertir posición de entrada en coordenadas locales
        Vector2 position = RectTransformUtility.WorldToScreenPoint(null, background.position);
        Vector2 direction = eventData.position - position;

        // Normalizar el vector de entrada
        inputVector = direction / handleRange;
        inputVector = inputVector.magnitude > 1.0f ? inputVector.normalized : inputVector;

        // Restringir movimiento a las cuatro direcciones principales
        if (Mathf.Abs(inputVector.x) > Mathf.Abs(inputVector.y))
        {
            inputVector.y = 0; // Priorizar movimiento en eje X
        }
        else
        {
            inputVector.x = 0; // Priorizar movimiento en eje Y
        }

        // Actualizar posición del joystick
        handle.anchoredPosition = new Vector2(
            inputVector.x * handleRange,
            inputVector.y * handleRange
        );

        // Actualizar animaciones (si aplica)
        if (animationController != null)
        {
            animationController.UpdateAnimationProgress(inputVector.x * handleRange, inputVector.y * handleRange);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        // Reiniciar joystick al soltarlo
        inputVector = Vector2.zero;
        handle.anchoredPosition = Vector2.zero;

        // Reiniciar animaciones (si aplica)
        if (animationController != null)
        {
            animationController.ResetAnimation();
        }
    }

    public Vector2 GetInput()
    {
        // Retorna el vector de entrada (dirección normalizada)
        return inputVector;
    }
}
