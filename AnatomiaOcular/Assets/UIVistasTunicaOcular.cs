using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVistasTunicaOcular : MonoBehaviour
{
    [Header("Botones Tunicas Oculares")]
    [SerializeField] private Image tunicaFibrosa;
    [SerializeField] private Image tunicaVascular;
    [SerializeField] private Image retina;
    [SerializeField] private Image mediosTransparentes;
    [Header("sprites botones")]
    [SerializeField] private Sprite enableTunicaFibrosaSprite;
    [SerializeField] private Sprite disableTunicaFibrosaSprite;
    [SerializeField] private Sprite enableTunicaVascularSprite;
    [SerializeField] private Sprite disableTunicaVascularSprite;
    [SerializeField] private Sprite enableRetinaSprite;
    [SerializeField] private Sprite disableRetinaSprite;
    [SerializeField] private Sprite enableMediosTransparentesSprite;
    [SerializeField] private Sprite disableMediosTransparentesSprite;
    [Header("Componentes grupos")]
    [SerializeField] private GameObject grupoTunicaFibrosa;
    [SerializeField] private GameObject grupoTunicaVascular;
    [SerializeField] private GameObject grupoRetina;
    [SerializeField] private GameObject grupoMediosTransparentes;
    [Header("Componentes tunica fibrosa")]
    [SerializeField] private Toggle toggleCornea;
    [SerializeField] private Toggle toggleEsclera;
    [SerializeField] private Toggle toggleMusculosOculares;
    [Header("Componentes tunica vascular")]
    [SerializeField] private Toggle toggleIris;
    [SerializeField] private Toggle toggleProcesoCiliar;
    [SerializeField] private Toggle togglemuculoCiliar;
    [SerializeField] private Toggle toggleCoroides;

    [Header("Componentes Retina")]
    [SerializeField] private Button btnOraSerrata;
    [SerializeField] private Button btnMacula;
    [SerializeField] private Button btnFovea;
    [SerializeField] private Button btnFoveola;
    [SerializeField] private Toggle toggleArteriaCentral;
    [SerializeField] private Toggle toggleVenaCentral;
    [SerializeField] private GameObject grupoRetinaEspecial;
    [Header("Componentes Medios Transparentes")]
    [SerializeField] private Toggle toggleCorneaMediosTransparentes;
    [SerializeField] private Toggle toggleCristalino;
    [SerializeField] private Toggle toggleCamaraAcuosaposterior;
    [SerializeField] private Toggle toggleCamaraAcuosaAnterior;
    [SerializeField] private Toggle toggleCamaraVitrea;

    [Header("Scripts")]
    [SerializeField] private UIWiki uIWiki;




    //Tunica Vascula
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SwitchSprite(string vista)
    {
        switch(vista)
        {
            case "TunicaFibrosa":
                tunicaFibrosa.sprite = enableTunicaFibrosaSprite;
                tunicaVascular.sprite = disableTunicaVascularSprite;
                retina.sprite = disableRetinaSprite;
                mediosTransparentes.sprite = disableMediosTransparentesSprite;
                //toggles
                grupoTunicaFibrosa.SetActive(true);
                grupoTunicaVascular.SetActive(false);
                grupoRetina.SetActive(false);
                grupoMediosTransparentes.SetActive(false);

                //toogles tunicafibrosa
                toggleCornea.isOn = true;
                toggleEsclera.isOn = true;
                toggleMusculosOculares.isOn = true;
                
                //Wiki
                uIWiki.SwitchSprite(vista);

                break;
            case "TunicaVascular":
                tunicaFibrosa.sprite = disableTunicaFibrosaSprite;
                tunicaVascular.sprite = enableTunicaVascularSprite;
                retina.sprite = disableRetinaSprite;
                mediosTransparentes.sprite = disableMediosTransparentesSprite;
                //toggles
                grupoTunicaFibrosa.SetActive(false);
                grupoTunicaVascular.SetActive(true);
                grupoRetina.SetActive(false);
                grupoMediosTransparentes.SetActive(false);

                //toggle tunica vascular
                toggleIris.isOn = true;
                toggleProcesoCiliar.isOn = true;
                togglemuculoCiliar.isOn = true;
                toggleCoroides.isOn = true;

                //Wiki
                uIWiki.SwitchSprite(vista);

                break;
            case "Retina":
                tunicaFibrosa.sprite = disableTunicaFibrosaSprite;
                tunicaVascular.sprite = disableTunicaVascularSprite;
                retina.sprite = enableRetinaSprite;
                mediosTransparentes.sprite = disableMediosTransparentesSprite;
                //toggles
                grupoTunicaFibrosa.SetActive(false);
                grupoTunicaVascular.SetActive(false);
                grupoRetina.SetActive(true);
                grupoMediosTransparentes.SetActive(false);

                //toggle retina
                toggleArteriaCentral.isOn = true;
                toggleVenaCentral.isOn = true;

                //Wiki
                uIWiki.SwitchSprite(vista);
                break;
            case "MediosTransparentes":
                tunicaFibrosa.sprite = disableTunicaFibrosaSprite;
                tunicaVascular.sprite = disableTunicaVascularSprite;
                retina.sprite = disableRetinaSprite;
                mediosTransparentes.sprite = enableMediosTransparentesSprite;
                //toggles
                grupoTunicaFibrosa.SetActive(false);
                grupoTunicaVascular.SetActive(false);
                grupoRetina.SetActive(false);
                grupoMediosTransparentes.SetActive(true);

                //toggle medios transparentes
                toggleCorneaMediosTransparentes.isOn = true;
                toggleCristalino.isOn = true;
                toggleCamaraAcuosaposterior.isOn = true;
                toggleCamaraAcuosaAnterior.isOn = true;
                toggleCamaraVitrea.isOn = true;

                //Wiki
                uIWiki.SwitchSprite(vista);
                
                break;
        }

    }

    public  void SpesialConponentInteractablesGloboOcular(bool interactable){
        toggleMusculosOculares.interactable = interactable;

        toggleMusculosOculares.gameObject.SetActive(interactable);

    }
    public void SpesialConponentInteractablesGloboSagital(bool interactable){
        togglemuculoCiliar.interactable = interactable;
        toggleArteriaCentral.interactable = interactable;
        toggleVenaCentral.interactable = interactable;

        togglemuculoCiliar.gameObject.SetActive(interactable);
        toggleArteriaCentral.gameObject.SetActive(interactable);   
        toggleVenaCentral.gameObject.SetActive(interactable);
        grupoRetinaEspecial.SetActive(interactable);
    }

    private void SetToggles(bool value){
        toggleCornea.isOn = value;
        toggleEsclera.isOn = value;
        toggleMusculosOculares.isOn = value;
        toggleIris.isOn = value;
        toggleProcesoCiliar.isOn = value;
        togglemuculoCiliar.isOn = value;
        toggleCoroides.isOn = value;
        toggleArteriaCentral.isOn = value;
        toggleVenaCentral.isOn = value;
        toggleCorneaMediosTransparentes.isOn = value;
        toggleCristalino.isOn = value;
        toggleCamaraAcuosaposterior.isOn = value;
        toggleCamaraAcuosaAnterior.isOn = value;
        toggleCamaraVitrea.isOn = value;
    }

}
