using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class toggleWikiScript : MonoBehaviour
{
    [SerializeField] private Sprite dropImage;
    [SerializeField] private Sprite upImage;
    [SerializeField] Image toogleImage;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SwitchSprite(bool state){
        if(state == true)
        toogleImage.sprite = upImage;
        else
        toogleImage.sprite = dropImage;
    }
}
