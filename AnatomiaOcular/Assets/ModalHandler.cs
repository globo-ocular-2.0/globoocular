using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Estructura
{
    public GameObject[] componentes;
    public GameObject[] subcomponentes;
    public Toggle[] toggles;
}

public class ModalHandler : MonoBehaviour
{
    [Header("Modelos Estructuras y Presets")]
    [SerializeField] private GameObject[] modelosEstructuras;
    [SerializeField] private GameObject[] preSetsButtons;

    [Header("Estructuras del Ojo Completo")]
    [SerializeField] private Estructura tunicaFibrosaOjoCompleto;
    [SerializeField] private Estructura tunicaVascularOjoCompleto;
    [SerializeField] private Estructura retinaOjoCompleto;
    [SerializeField] private Estructura mediosTransparentesOjoCompleto; // No tiene subcomponentes

    [Header("Estructuras del Ojo Corte")]
    [SerializeField] private Estructura tunicaFibrosaOjoCorte;
    [SerializeField] private Estructura tunicaVascularOjoCorte;
    [SerializeField] private Estructura retinaOjoCorte;
    [SerializeField] private Estructura mediosTransparentesOjoCorte; // No tiene subcomponentes

    [SerializeField] private bool isOjoCompleto = true;

    [SerializeField] private Dictionary<string, Estructura> ojoCompletoMap;
    [SerializeField] private Dictionary<string, Estructura> ojoCorteMap;

    [SerializeField] private ButtonsSpriteSwitcher buttonsSpriteSwitcher;


    private void Start()
    {
        InitializeEstructuras();
    }

    private void InitializeEstructuras()
    {
        // Diccionarios para asignar las estructuras
        ojoCompletoMap = new Dictionary<string, Estructura>()
        {
            { "TunicaFibrosa", tunicaFibrosaOjoCompleto },
            { "TunicaVascular", tunicaVascularOjoCompleto },
            { "Retina", retinaOjoCompleto },
            { "MediosTransparentes", mediosTransparentesOjoCompleto }
        };

        ojoCorteMap = new Dictionary<string, Estructura>()
        {
            { "TunicaFibrosa", tunicaFibrosaOjoCorte },
            { "TunicaVascular", tunicaVascularOjoCorte },
            { "Retina", retinaOjoCorte },
            { "MediosTransparentes", mediosTransparentesOjoCorte }
        };

    }

    public void IsOjoCompleto(bool state)
    {
        isOjoCompleto = state;
    }

    public void InitializationModels(){
        if (isOjoCompleto)
        {
            buttonsSpriteSwitcher.ActiveAllModelsOjoCompleto(true);
        }
        else
        {
            buttonsSpriteSwitcher.ActiveAllModelsOjoCorte(true);
        }

    }

    public void ShowAllModalsEstructuras(bool state)
    {
        var currentMap = isOjoCompleto ? ojoCompletoMap : ojoCorteMap;
        var oppositeMap = isOjoCompleto ? ojoCorteMap : ojoCompletoMap;

            foreach (var estructura in currentMap.Values)
            {
            SetActiveForAll(estructura.componentes, state);
            SetActiveForAll(estructura.subcomponentes, state);
            SetActiveForAll(estructura.toggles, state);
            }

            
            foreach (var estructura in oppositeMap.Values)
            {
            SetActiveForAll(estructura.componentes, !state);
            SetActiveForAll(estructura.subcomponentes, !state);
            SetActiveForAll(estructura.toggles, !state);
            }
    }
    

    public void ShowModeloEstructuras(int index)
    {
        SetActiveForAll(modelosEstructuras, false);
        modelosEstructuras[index].SetActive(true);

        SetActiveForAll(preSetsButtons, false);
        preSetsButtons[index].SetActive(true);
    }

    public void ShowComponentes(string estructuraKey)
    {
        var currentMap = isOjoCompleto ? ojoCompletoMap : ojoCorteMap;

        HideAllComponentes();
        HideAllSubcomponentes();


        if (currentMap.ContainsKey(estructuraKey))
        {
            Estructura estructura = currentMap[estructuraKey];
            SetActiveForAll(estructura.componentes, true);
            SetActiveForAll(estructura.toggles, true);

            if (estructura.subcomponentes != null)
            {
                SetActiveForAll(estructura.subcomponentes, true);
            }
        }
    }

    public void ShowTunicaFibrosaComponentes() => ShowComponentes("TunicaFibrosa");

    public void ShowTunicaVascularComponentes() => ShowComponentes("TunicaVascular");

    public void ShowRetinaComponentes() => ShowComponentes("Retina");

    public void ShowMediosTransparentesComponentes() => ShowComponentes("MediosTransparentes");

    public void HideAllComponentes()
    {
        HideComponents(ojoCompletoMap);
        HideComponents(ojoCorteMap);
    }

    public void HideAllSubcomponentes()
    {
        HideSubcomponents(ojoCompletoMap);
        HideSubcomponents(ojoCorteMap);
    }

    private void HideComponents(Dictionary<string, Estructura> estructuraMap)
    {
        foreach (var estructura in estructuraMap.Values)
        {
            SetActiveForAll(estructura.componentes, false);
        }
    }

    private void HideSubcomponents(Dictionary<string, Estructura> estructuraMap)
    {
        foreach (var estructura in estructuraMap.Values)
        {
            SetActiveForAll(estructura.subcomponentes, false);
        }
    }

    private void SetActiveForAll(GameObject[] objetos, bool estado)
    {
        if (objetos == null) return;

        foreach (var item in objetos)
        {
            item.SetActive(estado);
        }
    }

    private void SetActiveForAll(Toggle[] toggles, bool estado)
    {
        if (toggles == null) return;

        foreach (var item in toggles)
        {
            item.isOn = estado;
        }
    }
}
