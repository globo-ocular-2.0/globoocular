using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickController : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public RectTransform joystickBackground;
    public RectTransform joystickHandle;
    public RectTransform transformExterior;
    public RectTransform transformExteriorSuperior;
    public RectTransform transformExteriorInferior;
    public RectTransform transformInterior;
    public RectTransform transformInteriorSuperior;
    public RectTransform transformInteriorInferior;

    public Animator animator; // Asigna el Animator en el Inspector

    private Vector2 joystickCenter;
    [SerializeField] private float handleLimitX;
    [SerializeField] private float handleLimitY;
    private bool restrictToYAxis = false;
    private float fixedXPosition;

    [SerializeField] private TouchAndMouseGestures touchAndMouseGestures;

    void Start()
    {
        joystickCenter = joystickBackground.position;
        handleLimitX = joystickBackground.sizeDelta.x / 2;
        handleLimitY = joystickBackground.sizeDelta.y / 3f;
    }

    public void OnDrag(PointerEventData eventData)
    {
        DisableTouchAndMouseGestures(); // Desactiva touchAndMouseGestures durante el arrastre

        Vector2 direction = eventData.position - joystickCenter;
        float newXPosition, newYPosition;

        // Verifica si el joystick está cerca de las zonas de restricción
        bool nearExterior = Mathf.Abs(joystickHandle.position.x - transformExterior.position.x) < 50f;
        bool nearInterior = Mathf.Abs(joystickHandle.position.x - transformInterior.position.x) < 50f;

        if (nearExterior || nearInterior)
        {
            if (!restrictToYAxis)
            {
                // Guarda la posición X actual y activa la restricción al eje Y
                fixedXPosition = joystickHandle.position.x;
                restrictToYAxis = true;
            }
        }
        else
        {
            restrictToYAxis = false;
        }

        if (restrictToYAxis)
        {
            newYPosition = joystickCenter.y + Mathf.Clamp(direction.y, -handleLimitY, handleLimitY);
            joystickHandle.position = new Vector2(fixedXPosition, newYPosition);
        }
        else
        {
            newXPosition = joystickCenter.x + Mathf.Clamp(direction.x, -handleLimitX, handleLimitX);
            joystickHandle.position = new Vector2(newXPosition, joystickCenter.y);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        EnableTouchAndMouseGestures(); // Activa touchAndMouseGestures después del arrastre
        
        // Activa las animaciones según la posición del joystick
        TriggerAnimationIfNear(transformExterior, "recto_externo");
        TriggerAnimationIfNear(transformExteriorSuperior, "recto_superior_afuera");
        TriggerAnimationIfNear(transformExteriorInferior, "inferior_abajo_afuera");
        TriggerAnimationIfNear(transformInterior, "recto_medial_adentro");
        TriggerAnimationIfNear(transformInteriorSuperior, "oblicuo_arriba");
        TriggerAnimationIfNear(transformInteriorInferior, "oblicuo_abajo");

        // Restablece el estado y la posición del joystick
        joystickHandle.position = joystickCenter;
        restrictToYAxis = false;
    }

    private void DisableTouchAndMouseGestures()
    {
        if (touchAndMouseGestures != null)
        {
            touchAndMouseGestures.enabled = false;
        }
    }

    private void EnableTouchAndMouseGestures()
    {
        if (touchAndMouseGestures != null)
        {
            touchAndMouseGestures.enabled = true;
        }
    }

    private void TriggerAnimationIfNear(RectTransform targetTransform, string triggerName)
    {
        if (Vector2.Distance(joystickHandle.position, targetTransform.position) < 50f)
        {
            if (animator != null)
            {
                animator.SetTrigger(triggerName);
            }
        }
    }
}
