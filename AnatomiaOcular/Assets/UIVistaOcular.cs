using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVistaOcular : MonoBehaviour
{
    [Header("Main Buttons")]
    public Image btnGloboOcular;
    public Image btnGloboSagital;
    public Sprite selectedSprite;
    public Sprite unselectedSprite;

    [Header("Secondary Buttons")]
    public Image btnGloboOcularSec;
    public Image btnGloboSagitalSec;

    public Sprite selectedOcularSec;
    public Sprite unselectedOcularSec;
    public Sprite selectedSagitalSec;
    public Sprite unselectedSagitalSec;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SwitchSpriteVistaOcular(string vista)
    {
        switch(vista)
        {
            case "GloboOcular":
                btnGloboOcular.sprite = selectedSprite;
                btnGloboSagital.sprite = unselectedSprite;
                btnGloboOcularSec.sprite = selectedOcularSec;
                btnGloboSagitalSec.sprite = unselectedSagitalSec;
                break;
            case "GloboSagital":
                btnGloboOcular.sprite = unselectedSprite;
                btnGloboSagital.sprite = selectedSprite;
                btnGloboOcularSec.sprite = unselectedOcularSec;
                btnGloboSagitalSec.sprite = selectedSagitalSec;
                break;
        }
    }
    public void SwitchSpriteVistasTunicaOcular(string vista)
    {
        switch(vista)
        {
            case "TunicaFibrosa":
                btnGloboOcularSec.sprite = selectedOcularSec;
                btnGloboSagitalSec.sprite = unselectedSagitalSec;
                
                break;
            case "TunicaVascular":
                btnGloboOcularSec.sprite = unselectedOcularSec;
                btnGloboSagitalSec.sprite = selectedSagitalSec;
                break;
        }
    }
}
