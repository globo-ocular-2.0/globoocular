using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CameraModeManager : MonoBehaviour
{
    [Header("Camera Buttons")]
    [SerializeField] private Button[] cameraButtons;

    [Header("Camera Modes")]
    [SerializeField] private CameraMode[] cameraModes;

    [Header("Targets Position")]
    [SerializeField] private Transform[] targets;

    [Header("Modelos")]
    [SerializeField] private GameObject[] models;

    private ButtonManager buttonManager;
    private ModelManager modelManager;

    private void Start()
    {
        buttonManager = new ButtonManager(cameraButtons, Color.white, Color.green);
        modelManager = new ModelManager(models, targets);
        
        ChangeCameraMode(2); // Inicialmente se activa el modo de cámara 2
    }

    public void ChangeCameraMode(int index)
    {
        if (!IsValidIndex(index))
        {
            Debug.LogError("Índice fuera de rango.");
            return;
        }

        buttonManager.UpdateButtonColors(index);
        SwitchCameraMode(index);
        modelManager.MoveAndParentModels(index);
    }

    private bool IsValidIndex(int index)
    {
        return index >= 0 && index < cameraButtons.Length && index < targets.Length;
    }

    private void SwitchCameraMode(int index)
    {
        foreach (var mode in cameraModes)
        {
            if (mode.IsActiveForIndex(index))
            {
                mode.Activate();
            }
            else
            {
                mode.Deactivate();
            }
        }
    }
}

// Clase que maneja los botones de la UI
public class ButtonManager
{
    private Button[] buttons;
    private Color defaultColor;
    private Color selectedColor;

    public ButtonManager(Button[] buttons, Color defaultColor, Color selectedColor)
    {
        this.buttons = buttons;
        this.defaultColor = defaultColor;
        this.selectedColor = selectedColor;
    }

    public void UpdateButtonColors(int selectedIndex)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            SetButtonColor(buttons[i], i == selectedIndex);
        }
    }

    private void SetButtonColor(Button button, bool isSelected)
    {
        var textComponent = button.gameObject.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        if (textComponent != null)
        {
            textComponent.color = isSelected ? selectedColor : defaultColor;
        }
    }
}

// Clase que maneja los modelos y sus posiciones
public class ModelManager
{
    private GameObject[] models;
    private Transform[] targets;

    public ModelManager(GameObject[] models, Transform[] targets)
    {
        this.models = models;
        this.targets = targets;
    }

    public void MoveAndParentModels(int index)
    {
        Transform targetTransform = targets[index];
        foreach (var model in models)
        {
            model.transform.position = targetTransform.position;
            model.transform.SetParent(targetTransform, worldPositionStays: true);
        }
    }
}

// Clase que maneja un modo de cámara específico
[System.Serializable]
public class CameraMode
{
    [SerializeField] private GameObject cameraObject;
    [SerializeField] private int[] activeIndexes; // Índices para los cuales esta cámara está activa

    public bool IsActiveForIndex(int index)
    {
        return System.Array.Exists(activeIndexes, i => i == index);
    }

    public void Activate()
    {
        cameraObject.SetActive(true);
    }

    public void Deactivate()
    {
        cameraObject.SetActive(false);
    }
}
