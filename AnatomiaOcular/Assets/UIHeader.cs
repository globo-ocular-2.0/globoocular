using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIHeader : MonoBehaviour
{
    [Header("Main Buttons")]
    [SerializeField] private Button btnOcular;
    [SerializeField] private Button btnSagital;
    [SerializeField] private Image imageTunicasDelGloboOculares; 
    [SerializeField] private TMP_Text textTunicasDelGloboOculares;
    [SerializeField] private Image imageMovimientosOculares;
    [SerializeField] private TMP_Text textMovimientosOculares;
    [SerializeField] private Image imageWiki;
    [SerializeField] private TMP_Text textWiki;
    [SerializeField] private Image imageMenu;
    [SerializeField] private TMP_Text textMenu;
    [Header("Sprites")]
    [SerializeField] private Sprite selectTunicasDelGloboOculares;
    [SerializeField] private Sprite unselectTunicasDelGloboOculares;
    [SerializeField] private Sprite selectMovimientosOculares;
    [SerializeField] private Sprite unselectMovimientosOculares;
    [SerializeField] private Sprite selectWiki;
    [SerializeField] private Sprite unselectWiki;
    [SerializeField] private Sprite selectMenu;
    [SerializeField] private Sprite unselectMenu;
    [Header("Panels")]
    [SerializeField] private GameObject panelTunicasDelGloboOculares;
    [SerializeField] private GameObject panelMovimientosOculares;
    [SerializeField] private GameObject panelWiki;
    [SerializeField] private GameObject panelMenu;
    [Header("Scripts")]
    [SerializeField] private CameraHandler cameraHandler;
    [SerializeField] private VistaOcularController vistaOcularController;
    [SerializeField] private GameObject findePlane;

    [SerializeField] private TouchGestures touchGestureCompleto;
    [SerializeField] private TouchGestures touchGestureSagital;



    private bool isPanelEnabled = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void SwitchSprite(string vista)
    {
                
        if(cameraHandler.isAR)
        {
            SetTouchGestures(true);
            findePlane.SetActive(false);
        }else{
            SetTouchGestures(true);
        }
        switch (vista)
        {
            case "TunicasDelGloboOculares":
                // Change sprite and color
                imageTunicasDelGloboOculares.sprite = selectTunicasDelGloboOculares;
                textTunicasDelGloboOculares.color = new Color(0.416f, 0.4f, 0.949f, 1.0f);
                imageMovimientosOculares.sprite = unselectMovimientosOculares;
                textMovimientosOculares.color = new Color(255f, 255f, 255f, 1f);
                imageWiki.sprite = unselectWiki;
                textWiki.color = new Color(255f, 255f, 255f, 1.0f);
                imageMenu.sprite = unselectMenu;
                textMenu.color = new Color(255f, 255f, 255f, 1.0f);

                // Enable panel
                panelTunicasDelGloboOculares.SetActive(true);
                panelMovimientosOculares.SetActive(false);
                panelWiki.SetActive(false);
                panelMenu.SetActive(false);

                cameraHandler.MoveRightOffSet(true);

                InteractableButtons(true);


                //vistaOcularController.VistaGloboOcular("GloboOcular");

                break;
            case "MovimientosOculares":
                imageTunicasDelGloboOculares.sprite = unselectTunicasDelGloboOculares;
                textTunicasDelGloboOculares.color = new Color(255f, 255f, 255f, 1.0f);
                imageMovimientosOculares.sprite = selectMovimientosOculares;
                textMovimientosOculares.color = new Color(0.416f, 0.4f, 0.949f, 1.0f);
                imageWiki.sprite = unselectWiki;
                textWiki.color = new Color(255f, 255f, 255f, 1.0f);
                imageMenu.sprite = unselectMenu;
                textMenu.color = new Color(255f, 255f, 255f, 1.0f);

                panelTunicasDelGloboOculares.SetActive(false);
                panelMovimientosOculares.SetActive(true);
                panelWiki.SetActive(false);
                panelMenu.SetActive(false);

                cameraHandler.MoveRightOffSet(true);

                InteractableButtons(false);
                vistaOcularController.VistaGloboOcular("GloboOcular");

                

                break;
            case "Wiki":
                imageTunicasDelGloboOculares.sprite = unselectTunicasDelGloboOculares;
                textTunicasDelGloboOculares.color = new Color(255f, 255f, 255f, 1.0f);
                imageMovimientosOculares.sprite = unselectMovimientosOculares;
                textMovimientosOculares.color = new Color(255f, 255f, 255f, 1.0f);
                imageWiki.sprite = selectWiki;
                textWiki.color = new Color(0.416f, 0.4f, 0.949f, 1.0f);
                imageMenu.sprite = unselectMenu;
                textMenu.color = new Color(255f, 255f, 255f, 1.0f);

                panelTunicasDelGloboOculares.SetActive(false);
                panelMovimientosOculares.SetActive(false);
                panelWiki.SetActive(true);
                panelMenu.SetActive(false);

                cameraHandler.MoveLeftOffSet(true);


                break;
            case "Menu":
                imageTunicasDelGloboOculares.sprite = unselectTunicasDelGloboOculares;
                textTunicasDelGloboOculares.color = new Color(1f, 1f, 1f, 1f);
                imageMovimientosOculares.sprite = unselectMovimientosOculares;
                textMovimientosOculares.color = new Color(1f, 1f, 1f, 1.0f);
                imageWiki.sprite = unselectWiki;
                textWiki.color = new Color(1f, 1f, 1f, 1f);
                imageMenu.sprite = selectMenu;
                textMenu.color = new Color(0.416f, 0.4f, 0.949f, 1.0f);

                panelTunicasDelGloboOculares.SetActive(false);
                panelMovimientosOculares.SetActive(false);
                panelWiki.SetActive(false);
                panelMenu.SetActive(true);

                cameraHandler.MoveLeftOffSet(true);


                break;
        }
    }

    public void CloseAllPanels()
    {
        imageTunicasDelGloboOculares.sprite = unselectTunicasDelGloboOculares;
        textTunicasDelGloboOculares.color = new Color(1f, 1f, 1f, 1f);
        imageMovimientosOculares.sprite = unselectMovimientosOculares;
        textMovimientosOculares.color = new Color(1f, 1f, 1f, 1f);
        imageWiki.sprite = unselectWiki;
        textWiki.color = new Color(1f, 1f, 1f, 1f);
        imageMenu.sprite = unselectMenu;
        textMenu.color = new Color(1f, 1f, 1f, 1.0f);

        panelTunicasDelGloboOculares.SetActive(false);
        panelMovimientosOculares.SetActive(false);
        panelWiki.SetActive(false);
        panelMenu.SetActive(false);
        if(cameraHandler.isAR){
        findePlane.SetActive(true);
        SetTouchGestures(false);
        }else{
            SetTouchGestures(true);
            
        }

    }

    public void InteractableButtons(bool state)
    {
        btnOcular.interactable = state;
        btnSagital.interactable = state;
    }

    public void SetTouchGestures(bool state){
        touchGestureCompleto.enabled = state;
        touchGestureSagital.enabled = state;
    }
}
