using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsSpriteSwitcher : MonoBehaviour
{
    [SerializeField] private ModelViewManager modelViewManagerOjoCompleto;
    [SerializeField] private ModelViewManager modelViewManagerOjoCorte;
    [SerializeField] private ModalHandler modalHandler;
    [SerializeField] private Button[] buttons; 
    [SerializeField] private Sprite spriteOn;
    [SerializeField] private Sprite spriteOff;

    private void Start()
    {
        InitializeButtons(); // Inicializa todos los botones con el sprite "off"
    }

    // Método para inicializar los botones con el sprite "off"
    public void InitializeButtons()
    {
        SetAllButtonsSprite(spriteOff);
    }


    // Método para actualizar los sprites de todos los botones
    private void SetAllButtonsSprite(Sprite sprite)
    {
        foreach (Button btn in buttons)
        {
            SetButtonSprite(btn, sprite);
        }
    }

    // Método para cambiar el sprite de un solo botón
    private void SetButtonSprite(Button button, Sprite sprite)
    {
        Image img = button.GetComponent<Image>(); 
        img.sprite = sprite;
    }

    // Cambia los sprites de los botones al seleccionar una estructura
    public void SwitchSpriteEstructuras(int activeButtonIndex)
    {
        bool isOjoCompleto = activeButtonIndex == 0;
        modalHandler.IsOjoCompleto(isOjoCompleto);

        UpdateButtonSprites(activeButtonIndex);
    }

    // Cambia los sprites de los botones para Ojo Completo
    public void SwitchSpriteOjoCompleto(int activeButtonIndex)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            Image img = buttons[i].GetComponent<Image>();

            if (i == activeButtonIndex)
            {
                ToggleOjoCompletoButton(img);
            }
            else
            {
                SetButtonSprite(buttons[i], spriteOff);
            }
        }
    }

    // Método para actualizar los sprites de los botones según el índice activo
    private void UpdateButtonSprites(int activeButtonIndex)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            Sprite spriteToUse = (i == activeButtonIndex) ? spriteOn : spriteOff;
            SetButtonSprite(buttons[i], spriteToUse);
        }
    }

    // Alterna el sprite y la activación de los modelos en Ojo Completo
    private void ToggleOjoCompletoButton(Image img)
    {
        if (img.sprite == spriteOn)
        {
            SetButtonSprite(img.gameObject.GetComponent<Button>(), spriteOff);
            modelViewManagerOjoCompleto.ActiveAllModels(true);
            modelViewManagerOjoCorte.ActiveAllModels(true);
            modalHandler.ShowAllModalsEstructuras(true);
            Debug.Log("Desactivando imagen");
        }
        else
        {
            SetButtonSprite(img.gameObject.GetComponent<Button>(), spriteOn);
            Debug.Log("Activando imagen");
        }
    }

    public void ActiveAllModelsOjoCompleto(bool state)
    {
        modelViewManagerOjoCompleto.ActiveAllModels(state);

    }
    public void ActiveAllModelsOjoCorte(bool state)
    {
        modelViewManagerOjoCorte.ActiveAllModels(state);
    }
}
