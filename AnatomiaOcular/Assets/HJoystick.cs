using UnityEngine;
using UnityEngine.EventSystems;

public class HJoystick : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public RectTransform background; // Fondo del joystick
    public RectTransform handle;     // Joystick (manejador)
    
    [Header("Movimiento")]
    public float handleRange = 100f;  // Radio de movimiento del joystick
    public float joystickMinX = -200f; // Rango mínimo en X (izquierda)
    public float joystickMaxX = 200f;  // Rango máximo en X (derecha)
    public float joystickMinY = -100f; // Rango mínimo en Y (abajo)
    public float joystickMaxY = 100f;  // Rango máximo en Y (arriba)
    
    private Vector2 inputVector; // Vector de entrada
    private bool restrictToYAxis = false; // Restricción al eje Y
    private float fixedXPosition; // Posición fija en X cuando está en modo Y

    public JoystickAnimationController animationController; // Referencia al controlador de animaciones

    public void OnDrag(PointerEventData eventData)
    {
        // Convertir la posición del toque a coordenadas locales
        Vector2 position = RectTransformUtility.WorldToScreenPoint(null, background.position);
        Vector2 direction = eventData.position - position;

        // Normalizar entrada
        inputVector = direction / handleRange;
        inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

        float newX, newY;

        // Detectar si está cerca de los extremos para activar la restricción
        bool nearLeft = inputVector.x < -0.9f;
        bool nearRight = inputVector.x > 0.9f;

        if (restrictToYAxis || nearLeft || nearRight)
        {
            if (!restrictToYAxis)
            {
                // Fijar la posición X actual y activar el movimiento vertical
                fixedXPosition = inputVector.x * handleRange;
                restrictToYAxis = true;
            }

            // Movimiento vertical restringido
            newX = fixedXPosition;
            newY = Mathf.Clamp(inputVector.y * handleRange, joystickMinY, joystickMaxY);
        }
        else
        {
            // Movimiento horizontal normal
            restrictToYAxis = false;
            newX = Mathf.Clamp(inputVector.x * handleRange, joystickMinX, joystickMaxX);
            newY = 0;
        }

        // Actualizar posición del joystick
        handle.anchoredPosition = new Vector2(newX, newY);

        // Actualizar animaciones
        if (animationController != null)
        {
            animationController.UpdateAnimationProgress(newX, newY);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        // Resetear joystick al soltarlo
        inputVector = Vector2.zero;
        handle.anchoredPosition = Vector2.zero;
        restrictToYAxis = false;

        // Reiniciar animaciones
        if (animationController != null)
        {
            animationController.ResetAnimation();
        }
    }
}
