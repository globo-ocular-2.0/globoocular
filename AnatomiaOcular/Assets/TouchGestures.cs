using UnityEngine;
using UnityEngine.EventSystems; // Necesario para comprobar si se está tocando UI

public class TouchGestures : MonoBehaviour
{
    [Header("Sensibilidad")]
    [Tooltip("Velocidad de rotación al usar un dedo.")]
    public float rotationSpeed = 0.1f;

    [Tooltip("Velocidad de movimiento al usar dos dedos.")]
    public float moveSpeed = 0.01f;

    [Tooltip("Factor de sensibilidad para el escalado.")]
    public float scaleSpeed = 1.0f;

    [Header("Habilitar/Deshabilitar Funcionalidades")]
    [Tooltip("Activar o desactivar la rotación del modelo.")]
    public bool enableRotation = true;

    [Tooltip("Activar o desactivar el movimiento del modelo.")]
    public bool enableMovement = true;

    [Tooltip("Activar o desactivar el escalado del modelo.")]
    public bool enableScaling = true;

    private Vector3 lastTouchPosition; // Para almacenar la última posición del toque
    private float initialDistance;    // Para el cálculo del escalado
    private Vector3 initialScale;     // Escala inicial del objeto

    void Update()
    {
        if (IsTouchOverUI()) return; // Si se toca UI, no hacer nada

        if (Input.touchCount == 1 && enableRotation) // Un dedo: rotación
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                Vector2 delta = touch.deltaPosition;

                transform.Rotate(Vector3.up, -delta.x * rotationSpeed, Space.World);   // Rotar alrededor del eje Y
                transform.Rotate(Vector3.right, delta.y * rotationSpeed, Space.World); // Rotar alrededor del eje X
            }
        }
        else if (Input.touchCount == 2) // Dos dedos: mover y escalar
        {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);

            // Mover el objeto
            if (enableMovement && (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved))
            {
                Vector2 delta1 = touch1.deltaPosition;
                Vector2 delta2 = touch2.deltaPosition;
                Vector3 averageDelta = (delta1 + delta2) * 0.5f;

                transform.position += new Vector3(averageDelta.x, averageDelta.y, 0) * moveSpeed * Time.deltaTime;
            }

            // Escalar el objeto
            if (enableScaling)
            {
                float currentDistance = Vector2.Distance(touch1.position, touch2.position);

                if (touch1.phase == TouchPhase.Began || touch2.phase == TouchPhase.Began)
                {
                    initialDistance = currentDistance;
                    initialScale = transform.localScale;
                }
                else if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
                {
                    float scaleFactor = (currentDistance / initialDistance) - 1; // Cambio proporcional
                    transform.localScale = initialScale + initialScale * scaleFactor * scaleSpeed;
                }
            }
        }
    }

    // Método para comprobar si se está tocando un elemento de la UI
    private bool IsTouchOverUI()
    {
        if (Input.touchCount > 0)
        {
            foreach (Touch touch in Input.touches)
            {
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    return true;
                }
            }
        }
        return false;
    }
}
