using UnityEngine;
using UnityEngine.EventSystems;

public class HorizontalJoystick : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public RectTransform background; // Fondo del joystick
    public RectTransform handle;     // Manejador del joystick

    [Header("Movimiento")]
    public float handleRange = 100f;  // Radio de movimiento del joystick
    public float joystickMinX = -100f; // Rango mínimo en X (izquierda)
    public float joystickMaxX = 100f;  // Rango máximo en X (derecha)

    private Vector2 inputVector; // Vector de entrada

    public JoystickHorizontalAnimationController animationController; // Referencia al controlador de animaciones

    public void OnDrag(PointerEventData eventData)
    {
        // Convertir la posición del toque a coordenadas locales
        Vector2 position = RectTransformUtility.WorldToScreenPoint(null, background.position);
        Vector2 direction = eventData.position - position;

        // Normalizar entrada
        inputVector = direction / handleRange;
        inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

        // Movimiento horizontal restringido
        float newX = Mathf.Clamp(inputVector.x * handleRange, joystickMinX, joystickMaxX);

        // Actualizar posición del joystick (Y fijo en 0, solo se mueve en X)
        handle.anchoredPosition = new Vector2(newX, 0);

        // Actualizar animaciones
        if (animationController != null)
        {
            animationController.UpdateAnimationProgress(newX);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        // Resetear joystick al soltarlo
        inputVector = Vector2.zero;
        handle.anchoredPosition = Vector2.zero;

        // Reiniciar animaciones
        if (animationController != null)
        {
            animationController.ResetAnimation();
        }
    }
}
