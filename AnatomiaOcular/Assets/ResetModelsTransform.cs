using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetModelsTransform : MonoBehaviour
{
    [SerializeField] private GameObject[] models;

    public void ResetTransform()
    {
        foreach (var model in models)
        {
            model.transform.localPosition = Vector3.zero;
            model.transform.localRotation = Quaternion.identity;
            model.transform.localScale = Vector3.one;
        }
    }
}
